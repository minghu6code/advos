use core::ptr::null_mut;

use crate::kmem::{kfree, kmalloc};

pub mod virtio;

///////////////////////////////////////////////////////////////////////////////
//// Common Used Data Structure

/// Block Buffer
pub struct BlockBuffer {
    pub buffer: *mut u8
}

impl BlockBuffer {
    pub fn new(size: u32) -> Self {
        Self {
            buffer: kmalloc(size as usize).unwrap()
        }
    }
}

impl Default for BlockBuffer {
    fn default() -> Self {
        Self::new(1024)
    }
}

impl Drop for BlockBuffer {
    fn drop(&mut self) {
        if !self.buffer.is_null() {
            kfree(self.buffer);
            self.buffer = null_mut();
        }
    }
}

