
use core::mem::size_of;

use crate::*;
use crate::device::*;

///////////////////////////////////////////////////////////////////////////////
//// MMIO
// The MMIO transport is legacy in QEMU,
// so these register represent the legacy interface
#[repr(usize)]
pub enum MMIOSpec {
    MagicValue       = 0x000,
    Version          = 0x004,
    DeviceId         = 0x008,
    VendorId         = 0x00c,
    HostFeatures     = 0x010,
    HostFeaturesSel  = 0x014,
    GuestFeatures    = 0x020,
    GuestFeaturesSel = 0x024,
    GuestPageSize    = 0x028,
    QueueSel         = 0x030,
    QueueNumMax      = 0x034,
    QueueNum         = 0x038,
    QueueAlign       = 0x03c,
    QueuePFN         = 0x040,
    QueueNotify      = 0x050,
    InterruptStatus  = 0x060,
    interruptAck     = 0x064,
    Status           = 0x070,
    Config           = 0x100
}

impl MMIOSpec {
    pub fn scaled(self, scale: usize) -> usize {
        (self as usize) / scale
    }

    pub fn scale32(self) -> usize {
        self.scaled(4)
    }
}

pub enum MMIOStatus {
    Acknowledge     = 1 << 0,
    Driver          = 1 << 1,
    DriverOk        = 1 << 2,
    FeaturesOk      = 1 << 3,
    DeviceNeedReset = 1 << 6,
    Failed          = 1 << 7,
}


///////////////////////////////////////////////////////////////////////////////
//// VirtIO

/// DeviceDescriptor
/// ```text
/// The descriptor holds the data that we need to send to the device.
/// The address is a physical address.
/// The len is in bytes and the flags are specified above.
/// Any descriptor can be chained, hence the next field,
/// but only if the F_NEXT flag is specified.
/// ```
#[derive(Clone, Copy)]
#[repr(C)]
pub struct DevDesc {
    pub addr: u64,
    pub len: u32,
    pub flags: u16,
    pub next: u16
}

/// AvailableRing
#[derive(Clone, Copy)]
#[repr(C)]
pub struct AvaRing {
    pub flags: u16,
    pub idx:   u16,
    pub ring:  [u16; VIRTIO_RING_SIZE],
    pub event: u16,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct UsedRing {
    pub flags: u16,
    pub idx: u16,
    pub ring: [UsedElem; VIRTIO_RING_SIZE],
    pub event: u16
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct UsedElem {
    pub id: u32,
    pub len: u32
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct VirtQueue {
    pub descvec: [DevDesc; VIRTIO_RING_SIZE],
    pub avaring: AvaRing,
    // just for padding PAGE_SIZE
    pub padding: [u8; PAGE_SIZE - size_of::<DevDesc>() * VIRTIO_RING_SIZE - size_of::<AvaRing>()],
    pub usedring: UsedRing
}


///////////////////////////////////////////////////////////////////////////////
//// Device
#[derive(Clone, Copy)]
pub enum DeviceType {
    None = 0,
    Network = 1,
    Block = 2,
    Console = 3,
    Entropy = 4,
    GPU = 16,
    Input = 18,
    Memory = 24
}

#[derive(Clone, Copy)]
pub struct VirtIODevice {
    pub devtype: DeviceType
}

impl VirtIODevice {
    pub const fn new() -> Self {
        Self {
            devtype: DeviceType::None
        }
    }

    pub const fn with_type(devtype: DeviceType) -> Self {
        Self {
            devtype
        }
    }
}

static mut VIRTIO_DEVICES: [Option<VirtIODevice>; 8] = [None; 8];


/// Probe the VirtIO bus for devices
pub fn probe_device() {
    for addr in (MMIO_VIRTIO_START..=MMIO_VIRTIO_END).step_by(MMIO_VIRTIO_STRIDE) {
        print!("VirtIO probing 0x{:08x}...", addr);

        let magicvalue;
        let deviceid;
        let ptr = addr as *mut u32;
        unsafe {
            magicvalue = ptr.read_volatile();
            deviceid = ptr.add(2).read_volatile();
        }

        if MMIO_VIRTIO_MAGIC != magicvalue {
            println!("not virtio.");
        }
        else if deviceid == 0 {
            println!("not connected.");
        }
        else {
            match deviceid {
                1 => {  // network device
                    print!("network device...");
                    if setup_network_device(ptr) {
                        println!("setup succeeded!");
                    }
                    else {
                        println!("setup failed.");
                    }
                },
                2 => {  // block device
                    print!("block device...");
                    if setup_block_device(ptr) {
                        let idx = (addr - MMIO_VIRTIO_START) >> 12;
                        unsafe {
                            VIRTIO_DEVICES[idx]
                            = Some(VirtIODevice::with_type(DeviceType::Block));
                        }
                        println!("setup succeeded!");
                    }
                    else {
                        println!("setup failed.");
                    }
                },
                4 => {  // random number generator device
                    print!("entropy device...");
                    if setup_entropy_device(ptr) {
                        println!("setup succeeded!");
                    }
                    else {
                        println!("setup failed.");
                    }
                },
                16 => {  // GPU device
                    print!("GPU device...");
                    if setup_gpu_device(ptr) {
                        println!("setup succeeded!");
                    }
                    else {
                        println!("setup failed.");
                    }
                },
                18 => {  // input device
                    print!("input device...");
                    if setup_input_device(ptr) {
                        println!("setup succeeded!");
                    }
                    else {
                        println!("setup failed.");
                    }
                },
                _ => println!("unknown device type.")
            }
        }
    }
}

pub fn handle_interrupt(intr: u32) {
    let idx = intr as usize - 1;
    unsafe {
        if let Some(vd) = &VIRTIO_DEVICES[idx] {
            match vd.devtype {
                DeviceType::Block => {
                    block::handle_interrupt(idx);
                },
                _ => {
                    println!("Invalid device generated interrupt!");
                }
            }
        }
        else {
            println!("Spurious interrupt {}", intr);
        }
    }
}
