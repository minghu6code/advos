
use crate::uart::UART;
use crate::io::virtio;
use crate::*;

/// Enable a given interrupt id
// Unlike the complete and claim registers, the plic_int_enable
// register is a bitset where the id is the bit index. The register
// is a 32-bit register, so that gives us enables for interrupts
// 31 through 1 (0 is hardwired to 0).
pub fn enable(id: u32) {
    let enable_reg = PLIC_INT_ENABLE as *mut u32;
    let actualid = 1 << id;

    unsafe {
        enable_reg.write_volatile(
            enable_reg.read_volatile() | actualid
        );
    }
}

/// Set a given interrupt priority to the given priority.
/// The priority must be [0..7] (from low priority through high priority)
pub fn set_priority(id: u32, priority: u32) {
    let priority_reg = PLIC_PRIORITY as *mut u32;

    unsafe {
        priority_reg.add(id as usize).write_volatile(
            priority
        );
    }
}

/// Set the global threshold. The threshold can be a value [0..7].
/// The PLIC will mask any interrupts at or below the given threshold.
/// This means that a threshold of 7 will mask ALL interrupts and
/// a threshold of 0 will allow ALL interrupts.
pub fn set_threshold(tsh: u32) {
    let tsh_reg = PLIC_THRESHOLD as *mut u32;

    unsafe {
        tsh_reg.write_volatile(tsh);
    }
}

/// Get the next available interrupt. This is the "claim" process.
/// The plic will automatically sort by priority and hand us the
/// ID of the interrupt. For example, if the UART is interrupting
/// and it's next, we will get the value 10.
pub fn next() -> Option<u32> {
    let claim_reg = PLIC_CLAIM as *const u32;

    unsafe {
        match claim_reg.read_volatile()  {
            0 => None,
            other => Some(other)
        }
    }
}


/// Complete a pending interrupt by id. The id should come
/// from the next() function above (orelse would be ignored by cpu).
pub fn complete(id: u32) {
    let complete_reg = PLIC_COMPLETE as *mut u32;

    unsafe {
        complete_reg.write_volatile(id);
    }
}


pub fn handle_interrupt() {
    if let Some(intr) = plic::next() {
        match intr {
            // Interrupt 10 is the UART interrupt.
            // We would typically set this to be handled out of the interrupt context,
            // but we're testing here! C'mon!
            // We haven't yet used the singleton pattern for my_uart, but remember, this
            // just simply wraps 0x1000_0000 (UART).
            1..=8 => {
                virtio::handle_interrupt(intr)
            },
            10 => {
                let mut my_uart = UART::new(UART0);
                // If we get here, the UART better have something! If not, what happened??
                if let Some(c) = my_uart.get() {
                    match c {
                        8 => {
                            print!("{} {}", 8 as char, 8 as char);
                        },
                        10 | 13 => {
                            println!();
                        },
                        _ => {
                            print!("{}", c as char);
                        },
                    }
                }
            },
            _ => {
                println!("Non-UART external interrupt: {}", intr);
            }
        }
        // We've claimed it, so now say that we've handled it. This resets the interrupt pending
        // and allows the UART to interrupt again. Otherwise, the UART will get "stuck".
        plic::complete(intr);
    }
}