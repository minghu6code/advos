
use core::convert::TryInto;
use core::fmt::Write;
use core::fmt::Result;

use crate::r#const::*;

pub fn init_uart() {
    let mut uart = UART::new(UART0);
    unsafe {
        uart.init();
    };
}

#[derive(Clone)]
pub struct UART {
    adr_base: usize
}


impl UART {
    pub fn new(adr_base: usize) -> Self {
        Self {
            adr_base
        }
    }

    pub unsafe fn init(&mut self) {
        let ptr = self.adr_base as *mut u8;

        // +3: LCR
        // According to the technical specifications, if we write a 1 in each of these slots,
        // we get an 8-bit character length.
        // By default,
        // if we write a 0 into each slot, we get a 5-bit word length, which is just weird.
        ptr.add(3).write_volatile(0b_0000_0001 | 0b_0000_0010);

        // Enable FIFO
        ptr.add(2).write_volatile(0b_0000_0001);

        // Enable receiver buffer interrupts
        ptr.add(1).write_volatile(0b_0000_0001);

        // If we cared about the divisor, the code below would set the divisor
        // from a global clock rate of 22.729 MHz (22,729,000 cycles per second)
        // to a signaling rate of 2400 (BAUD). We usually have much faster signalling
        // rates nowadays, but this demonstrates what the divisor actually does.
        // The formula given in the NS16500A specification for calculating the divisor
        // is:
        // divisor = ceil( (clock_hz) / (baud_sps x 16) )
        // So, we substitute our values and get:
        // divisor = ceil( 22_729_000 / (2400 x 16) )
        //         = ceil( 22_729_000 / 38_400 )
        //         = ceil( 591.901 ) = 592

        // The divisor register is two bytes (16 bits), so we need to split the value
        // 592 into two bytes. Typically, we would calculate this based on measuring
        // the clock rate, but again, for our purposes [qemu], this doesn't really do
        // anything.
        let divisor: u16 = 592;
        let divisor_least: u8 = ((divisor & 0xff)).try_into().unwrap();
        let divisor_most:  u8 = ((divisor >> 8)).try_into().unwrap();
        // Notice that the divisor register DLL (divisor latch least) and DLM (divisor latch most)
        // have the same base address as the receiver/transmitter and the interrupt enable register.
        // To change what the base address points to, we open the "divisor latch" by writing 1 into
        // the Divisor Latch Access Bit (DLAB), which is bit index 7 of the Line Control Register (LCR)
        // which is at base_address + 3.
        let lcr = ptr.add(3).read_volatile();  // save lcr
        ptr.add(3).write_volatile(lcr | 1 << 7);
        // Now, base addresses 0 and 1 point to DLL and DLM, respectively.
        // Put the lower 8 bits of the divisor into DLL
        ptr.add(0).write_volatile(divisor_least);
        ptr.add(1).write_volatile(divisor_most);
        // Now that we've written the divisor, we never have to touch this again. In hardware, this
        // will divide the global clock (22.729 MHz) into one suitable for 2,400 signals per second.
        // So, to once again get access to the RBR/THR/IER registers, we need to close the DLAB bit
        // by clearing it to 0. Here, we just restore the original value of lcr.
        ptr.add(3).write_volatile(lcr);
    }

    pub fn put(&mut self, c: u8) {
        unsafe {
            (self.adr_base as *mut u8).add(0).write_volatile(c);
        }
    }

    pub fn get(&self) -> Option<u8> {
        let ptr = self.adr_base as *mut u8;

        unsafe {
            // if the DR bit is 0, meaning no data
            if ptr.add(5).read_volatile() & 1 == 0 {
                None
            } else {
            // else read data
                Some(ptr.add(0).read_volatile())
            }
        }
    }

    pub fn adr_base(&self) -> usize {
        self.adr_base
    }
}


impl Write for UART {
    fn write_str(&mut self, s: &str) -> Result {
        s.bytes().for_each(|c| {
            self.put(c);
        });

        Ok(())
    }
}