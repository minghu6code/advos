#![allow(unused)]

use core::{
    ptr::null
};


#[macro_export]
macro_rules! round_up_div {
    ($divisor:ident, $dividend:ident) => {
        ($divisor + $dividend - 1) / $dividend
    };
    ($divisor:expr, $dividend:ident) => {
        ($divisor + $dividend - 1) / $dividend
    };
    ($divisor:expr, $dividend:expr) => {
        let _dividend = $dividend;
        round_up_div!($divisor, $_dividend)
    };
    ($divisor:ident, $dividend:expr) => {
        let _dividend = $dividend;
        round_up_div!($divisor, $_dividend)
    };
}


/// This function always rounds up.
pub const fn round_up_align(val: usize, lsbits: usize) -> usize {
    let o = (1usize << lsbits) - 1;

    (val + o) & !o
}

pub const fn round_up_align4k(val: usize) -> usize {
    round_up_align(val, 12)
}

pub const fn align(val: usize, lsbits: usize) -> usize {
    val & !((1usize << lsbits) - 1)
}

pub const fn align4k(val: usize) -> usize {
    align(val, 12)
}

pub const fn align8(val: usize) -> usize {
    align(val, 3)
}

/// bit: 1 true, bit: 0 false
pub const fn u8ind(val: u8, i: u8) -> bool {
    let mask: u8 = 0 | (1 << i);

    (val & mask) > 0
}

pub fn u8set(ptr: *mut u8, i: u8) {
    unsafe { *ptr |= (1 << i) }
}

pub fn u8unset(ptr: *mut u8, i: u8) {
    unsafe { *ptr &= !(1 << i) }
}

pub fn clear_zero(addr: *const u8, u8len: usize) {
    let u64ptr = addr as *mut u64;
    let u64len = u8len / 8;
    let rem = u8len % 8;

    unsafe {
        for i in 0..u64len {
            *(u64ptr.add(i)) = 0;
        }

        let u8ptr = (u64ptr.add(u64len)) as *mut u8;

        for i in 0..rem {
            *(u8ptr.add(i)) = 0;
        }
    }
}

pub unsafe fn ptr_bin_find(
    begin: *const u8,
    end: *const u8,
    bytes_width: usize,
    matcher: impl Fn(*const u8) -> bool
) -> *const u8
{
    let len = end as usize- begin as usize;

    if begin > end {
        return null();
    }

    let mid = begin.add(len / 2);
    if matcher(mid) {
        return mid;
    }

    let found_ptr = ptr_bin_find(begin, mid, bytes_width, &matcher);

    if found_ptr != null() {
        return found_ptr;
    }

    ptr_bin_find(mid.add(1), end, bytes_width, &matcher)
}
