use alloc::{fmt, string::String};
use core::mem::size_of;

use crate::fs::minix3::Inode;

///////////////////////////////////////////////////////////////////////////////
//// uart.rs
pub const UART0: usize = 0x1000_0000;

///////////////////////////////////////////////////////////////////////////////
//// page.rs
pub const PAGE_SIZE: usize = 1 << 12;  // 4K

///////////////////////////////////////////////////////////////////////////////
//// timer
pub const MTIMECMP: usize = 0x0200_4000;
pub const MTIME: usize = 0x0200_bff8;
pub const CONTEXT_SWITCH_TIME: u64 = 10_000_000 / 250;

///////////////////////////////////////////////////////////////////////////////
//// plic.rs
pub const PLIC_PRIORITY: usize = 0x0c00_0000;
pub const PLIC_PENDING: usize = 0x0c00_1000;
pub const PLIC_INT_ENABLE: usize = 0x0c00_2000;
pub const PLIC_THRESHOLD: usize = 0x0c20_0000;
pub const PLIC_CLAIM: usize = 0x0c20_0004;

// complete register and claim register is the same register.
pub const PLIC_COMPLETE: usize = PLIC_CLAIM;

pub const PLIC_START: usize = PLIC_PRIORITY;
pub const PLIC_END: usize = PLIC_CLAIM;


///////////////////////////////////////////////////////////////////////////////
//// proc.rs

// How many pages are we going to give a process for their
// stack?
pub const PROC_STACK_PAGES: usize = 5;
// We want to adjust the stack to be at the bottom of the memory allocation
// regardless of where it is on the kernel heap.
pub const PROC_STACK_ADDR: usize = 0x1_0000_0000;
pub const PROC_STARTING_ADDR: usize = 0x2000_0000;


///////////////////////////////////////////////////////////////////////////////
//// virio.rs
pub const MMIO_VIRTIO_START: usize = 0x1000_1000;
pub const MMIO_VIRTIO_END: usize = 0x1000_8000;
pub const MMIO_VIRTIO_STRIDE: usize = 0x1000;
// for little endian that's rev(virt): t-r-i-v
pub const MMIO_VIRTIO_MAGIC: u32 = 0x74_72_69_76;

// According to the documentation, this must be a power
// of 2 for the new style. So, I'm changing this to use
// 1 << instead because that will enforce this standard.
pub const VIRTIO_RING_SIZE: usize = 1 << 7;

// helper
#[inline]
pub fn get_device_id(ptr: *mut u32) -> usize {
    (ptr as usize - MMIO_VIRTIO_START) >> 12
}

// Flags
pub const VIRTIO_DESC_F_NEXT: u16 = 1;
pub const VIRTIO_DESC_F_WRITE: u16 = 2;
pub const VIRTIO_DESC_F_INDIRECT: u16 = 4;
pub const VIRTIO_AVAIL_F_NO_INTERRUPT: u16 = 1;
pub const VIRTIO_USED_F_NO_NOTIFY: u16 = 1;

///////////////////////////////////////////////////////////////////////////////
//// device/block.rs
// Type values
pub const VIRTIO_BLK_T_IN: u32 = 0;
pub const VIRTIO_BLK_T_OUT: u32 = 1;
pub const VIRTIO_BLK_T_FLUSH: u32 = 4;
pub const VIRTIO_BLK_T_DISCARD: u32 = 11;
pub const VIRTIO_BLK_T_WRITE_ZEROES: u32 = 13;

// Status values
pub const VIRTIO_BLK_S_OK: u8 = 0;
pub const VIRTIO_BLK_S_IOERR: u8 = 1;
pub const VIRTIO_BLK_S_UNSUPP: u8 = 2;

// Feature bits
pub const VIRTIO_BLK_F_SIZE_MAX: u32 = 1;
pub const VIRTIO_BLK_F_SEG_MAX: u32 = 2;
pub const VIRTIO_BLK_F_GEOMETRY: u32 = 4;
pub const VIRTIO_BLK_F_RO: u32 = 5;
pub const VIRTIO_BLK_F_BLK_SIZE: u32 = 6;
pub const VIRTIO_BLK_F_FLUSH: u32 = 9;
pub const VIRTIO_BLK_F_TOPOLOGY: u32 = 10;
pub const VIRTIO_BLK_F_CONFIG_WCE: u32 = 11;
pub const VIRTIO_BLK_F_DISCARD: u32 = 13;
pub const VIRTIO_BLK_F_WRITE_ZEROES: u32 = 14;

pub const SECTOR_SIZE: u64 = 512;

///////////////////////////////////////////////////////////////////////////////
//// minix3.rs
pub const MINIX3_MAGIC: u16 = 0x4d5a;
pub const MINIX3_BLOCK_SIZE: u32 = 1024;  // unit: bnytes
pub const MINIX_NUM_INODES_PER_BLOCK: u32
= MINIX3_BLOCK_SIZE / size_of::<Inode>() as u32;


///////////////////////////////////////////////////////////////////////////////
//// Trap Code

#[derive(Clone, PartialEq, Eq)]
pub struct TrapError {
    pub code: TrapCode,
    pub msg: String
}

impl fmt::Debug for TrapError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "code: {:?}, {}", self.code, self.msg)
    }
}


#[derive(Clone, PartialEq, Eq, Debug)]
pub enum TrapCode {
    // PID
    PidDoubleRecycle,

    // FS
    FileNotFound,
    FilePermission,
    IsFile,
    IsDir,

    // Proc
    UnInitializedProcList,

    // Block
    BlockDeviceNotFound,
    WriteToReadOnly,

    InvalidArgument,

}

