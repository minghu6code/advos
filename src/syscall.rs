use crate::cpu::{TrapFrame, dump_register};
use crate::*;
use crate::device::block::{BlkIO, block_io};
use crate::fs::minix3::new_read_process;
use crate::proc::{get_proc, rm_proc, set_sleeping, set_waiting};

pub unsafe fn do_syscall(mepc: usize, frame: *mut TrapFrame) -> usize {
    let syscall_number;
    unsafe {
        // Libgloss's convention expect:
        // syscall number is saved in a7(x17) register
        syscall_number = (*frame).regs[17];
    }

    match syscall_number {
        0 | 93 => {  // Exit
            rm_proc((*frame).pid as pid_t);
            0
        },
        1 => {
            println!("Test syscall");
            mepc + 4
        },
        2 => {  // putchar
            print!("{}", (*frame).regs[10] as u8 as char);
            mepc + 4
        },
        8 => {
            dump_register(frame);
            mepc + 4
        },
        10 => {
            set_sleeping((*frame).pid as pid_t, (*frame).regs[10]);
            0
        },
        63 => {  // Read
            let mut phy_buffer = (*frame).regs[12];

            if (*frame).satp != 0 {
                let proc = get_proc((*frame).pid as pid_t).unwrap();
                let table = proc.get_table_addr() as *mut PageTable;

                if let Some(addr) = vir2phy(&*table, (*frame).regs[12]) {
                    phy_buffer = addr;
                }
                else {
                    (*frame).regs[10] = -1isize as usize;
                    return mepc + 4;
                }
            }

            new_read_process(
                (*frame).pid as pid_t,
                (*frame).regs[10] as usize,
                (*frame).regs[11] as u32,
                phy_buffer as *mut u8,
                (*frame).regs[13] as u32,
                (*frame).regs[14] as u32
            );

            0
        },
        180 => {
            set_waiting((*frame).pid as pid_t);

            block_io(
                (*frame).regs[10],
                (*frame).regs[11] as *mut u8,
                (*frame).regs[12] as u32,
                (*frame).regs[13] as u64,
                BlkIO::R,
                (*frame).pid as pid_t
            );

            0
        },
        _ => {
            println!("Unknown syscall number {}", syscall_number);
            mepc + 4
        }
    }
}

extern "C" {
    fn make_syscall(sysno: usize,
                    arg0: usize,
                    arg1: usize,
                    arg2: usize,
                    arg3: usize,
                    arg4: usize,
                    arg5: usize)
                    -> usize;
}

fn rust_make_syscall(
    sysno: usize,
    arg0: usize,
    arg1: usize,
    arg2: usize,
    arg3: usize,
    arg4: usize,
    arg5: usize)
    -> usize
{
    unsafe {
        make_syscall(sysno, arg0, arg1, arg2, arg3, arg4, arg5)
     }
}

// pub fn syscall_test() {
//     rust_make_syscall(1, 0, 0, 0, 0, 0, 0);
// }

pub fn syscall_block_read(dev: usize, buffer: *mut u8, size: u32, offset: u32) -> u8 {
    rust_make_syscall(
        180,
        dev,
        buffer as usize,
        size as usize,
        offset as usize,
        0,
        0
    ) as u8
}

pub fn syscall_fs_read(dev: usize, inode: u32, buffer: *mut u8, size: u32, offset: u32)
-> usize
{
    rust_make_syscall(
        63,
        dev,
        inode as usize,
        buffer as usize,
        size as usize,
        offset as usize,
        0
    )
}

pub fn syscall_exit() {
    rust_make_syscall(93, 0, 0, 0, 0, 0, 0);
}

///////////////////////////////////////////////////////////////////////////////
//// Syscall Reference

// These system call numbers come from libgloss so that we can use newlib
// for our system calls.
// Libgloss wants the system call number in A7 and arguments in A0..A6
// #define SYS_getcwd 17
// #define SYS_dup 23
// #define SYS_fcntl 25
// #define SYS_faccessat 48
// #define SYS_chdir 49
// #define SYS_openat 56
// #define SYS_close 57
// #define SYS_getdents 61
// #define SYS_lseek 62
// #define SYS_read 63
// #define SYS_write 64
// #define SYS_writev 66
// #define SYS_pread 67
// #define SYS_pwrite 68
// #define SYS_fstatat 79
// #define SYS_fstat 80
// #define SYS_exit 93
// #define SYS_exit_group 94
// #define SYS_kill 129
// #define SYS_rt_sigaction 134
// #define SYS_times 153
// #define SYS_uname 160
// #define SYS_gettimeofday 169
// #define SYS_getpid 172
// #define SYS_getuid 174
// #define SYS_geteuid 175
// #define SYS_getgid 176
// #define SYS_getegid 177
// #define SYS_brk 214
// #define SYS_munmap 215
// #define SYS_mremap 216
// #define SYS_mmap 222
// #define SYS_open 1024
// #define SYS_link 1025
// #define SYS_unlink 1026
// #define SYS_mkdir 1030
// #define SYS_access 1033
// #define SYS_stat 1038
// #define SYS_lstat 1039
// #define SYS_time 1062
// #define SYS_getmainvars 2011
