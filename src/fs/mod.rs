use crate::*;

pub mod minix3;


///////////////////////////////////////////////////////////////////////////////
//// Data Structure

pub struct Stats {
    pub mode: u16,
    pub size: u32,
    pub uid: u16,
    pub gid: u16
}

pub struct FileDesc {
    pub blkdev: usize,
    pub node: u32,
    pub loc: u32,
    pub size: u32,
    pub pid: pid_t
}


///////////////////////////////////////////////////////////////////////////////
//// File System Trait

pub trait FS {
    fn init(dev: usize) -> bool;
    fn open(path: &str) -> Result<FileDesc, TrapError>;
    fn read(desc: &FileDesc, buffer: *mut u8, size: u32, offset: u32) -> u32;
    fn write(desc: &FileDesc, buffer: *mut u8, size: u32, offset: u32) -> u32;
    fn close(desc: &FileDesc);
    fn stats(desc: &FileDesc) -> Stats;
}
