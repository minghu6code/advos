//! Minix 3 Filesystem Implementation

use core::mem::size_of;
use core::ptr;

use crate::proc::{Process, get_proc, set_running, set_waiting};
use crate::*;
use crate::io::BlockBuffer;
use crate::syscall::syscall_block_read;
use crate::kmem::talloc;

use super::{FS, FileDesc, Stats};


///////////////////////////////////////////////////////////////////////////////
//// Data Structure

/// The superblock describes the file system on the disk. It gives
/// us all the information we need to read the file system and navigate
/// the file system, including where to find the inodes and zones (blocks).
#[repr(C)]
pub struct SuperBlock {
    pub num_inodes:    u32,
        _padding_0:    u16,
    pub imap_blocks:   u16,
    pub zmap_blocks:   u16,
    pub fstdata_zone:  u16,
    pub log_zone_size: u16,
        _padding_1:    u16,
    pub max_size:      u32,
    pub zones:         u32,
    pub magicvalue:    u16,
        _padding_2:    u16,
    pub block_size:    u16,
    pub disk_ver:      u8
}

/// An inode (index node) stores the "meta-data" to a file.
#[derive(Clone, Copy)]
#[repr(C)]
pub struct Inode {
    pub mode: u16,
    pub num_links: u16,
    pub uid: u16,
    pub gid: u16,
    pub size: u32,
    pub atime: u32,  // access time
    pub mtime: u32,  // modified time
    pub ctime: u32,  // create time
    pub zones: [u32; 10]
}

/// Notice that an inode does not contain the name of a file. This is because
/// more than one file name may refer to the same inode. These are called "hard links"
/// Instead, a DirEntry essentially associates a file name with an inode as shown in
/// the structure below.
#[repr(C)]
pub struct DirEntry {
    pub inode: u32,
    pub name: [u8; 60]
}

pub struct Minix3Fs;

///////////////////////////////////////////////////////////////////////////////
//// MinixFileSystem Implmentation

impl Minix3Fs {
    pub fn get_inode(desc: &FileDesc, num_inode: u32) -> Option<*const Inode> {
        unsafe {
        let mut buffer = BlockBuffer::new(1024);

        syscall_block_read(desc.blkdev, buffer.buffer, 512, 1024);

        let superblock = buffer.buffer as *mut SuperBlock;
        let inode = buffer.buffer as *mut Inode;

        if (*superblock).magicvalue == MINIX3_MAGIC {
            // before inode zones
            let num_blocks_before_inodes
            = (2 + (*superblock).imap_blocks + (*superblock).zmap_blocks) as u32;

            // The inode comes to us as a NUMBER, not an index. So, we need to subtract 1.
            let num_blocks_inner_inodes
            = (num_inode - 1) / MINIX_NUM_INODES_PER_BLOCK;

            let inode_offset
            = (num_blocks_before_inodes + num_blocks_inner_inodes) * MINIX3_BLOCK_SIZE;

            // within inode zones
            let inode_inner_offset
            = ((num_inode - 1) % MINIX_NUM_INODES_PER_BLOCK) as usize;

            syscall_block_read(
                desc.blkdev,
                buffer.buffer,
                MINIX3_BLOCK_SIZE,
                inode_offset
            );

            return Some(inode.add(inode_inner_offset));
        }

        None
        }
    }
}


impl FS for Minix3Fs {
    fn init(dev: usize) -> bool {
        false
    }

    fn open(path: &str) -> Result<FileDesc, TrapError> {
        todo!()
    }

    fn read(desc: &FileDesc, buffer: *mut u8, size: u32, offset: u32) -> u32 {
        let mut blocks_seen = 0u32;
        let offset_block = offset / MINIX3_BLOCK_SIZE;
        let mut offset_bytes = offset % MINIX3_BLOCK_SIZE;
        let num_indirect_ptr = MINIX3_BLOCK_SIZE as usize / 4;

        let inode;
        if let Some(_inode) = Self::get_inode(desc, desc.node) {
            inode = unsafe { &*_inode };
        }
        else {
            return 0;
        }

        let mut bytes_rem = if size > inode.size { inode.size } else { size };
        let mut bytes_read = 0u32;
        let mut block_buffer = BlockBuffer::new(MINIX3_BLOCK_SIZE);
        let mut i1direct_buffer = BlockBuffer::new(MINIX3_BLOCK_SIZE);
        let mut i2direct_buffer = BlockBuffer::new(MINIX3_BLOCK_SIZE);
        let mut i3direct_buffer = BlockBuffer::new(MINIX3_BLOCK_SIZE);

        let i1zones = i1direct_buffer.buffer as *const u32;
        let i2zones = i2direct_buffer.buffer as *const u32;
        let i3zones = i3direct_buffer.buffer as *const u32;

        ///////////////////////////////////////////////
        ///// DIRECT ZONES
        ///////////////////////////////////////////////
        for i in 0..7 {
            // Any zone that has the value 0 is skipped and we check the next zones.
            // This might happen as we start writing and truncating.
            if inode.zones[i] == 0 {
                continue;
            }

            if offset_block <= blocks_seen {
                syscall_block_read(
                    desc.blkdev,
                    block_buffer.buffer,
                    MINIX3_BLOCK_SIZE,
                    inode.zones[i] * MINIX3_BLOCK_SIZE
                );

                let readbytes
                = if MINIX3_BLOCK_SIZE - offset_bytes > bytes_rem {
                    bytes_rem
                }
                else {
                    MINIX3_BLOCK_SIZE - bytes_rem
                };

                unsafe {
                    ptr::copy_nonoverlapping(
                        block_buffer.buffer.add(offset_bytes as usize),
                        buffer.add(bytes_read as usize),
                        readbytes as usize
                    );
                }

                offset_bytes = 0;
                bytes_read += readbytes;
                bytes_rem -= readbytes;

                if bytes_rem == 0 {
                    return bytes_read;
                }
            }

            blocks_seen += 1;
        }

        ///////////////////////////////////////////////
        ///// SINGLY INDIRECT ZONES
        ///////////////////////////////////////////////
        if inode.zones[7] != 0 {
            syscall_block_read(
                desc.blkdev,
                i1direct_buffer.buffer,
                MINIX3_BLOCK_SIZE,
                inode.zones[7] * MINIX3_BLOCK_SIZE
            );

            for i in 0..num_indirect_ptr {
                let i1zone_offset = unsafe {
                    i1zones.add(i).read()
                };

                if i1zone_offset == 0 { continue; }

                if offset_block <= blocks_seen {
                    syscall_block_read(
                        desc.blkdev,
                        block_buffer.buffer,
                        MINIX3_BLOCK_SIZE,
                        i1zone_offset * MINIX3_BLOCK_SIZE
                    );

                    let readbytes
                    = if MINIX3_BLOCK_SIZE - offset_bytes > bytes_rem {
                        bytes_rem
                    }
                    else {
                        MINIX3_BLOCK_SIZE - bytes_rem
                    };

                    unsafe {
                        ptr::copy_nonoverlapping(
                            block_buffer.buffer.add(offset_bytes as usize),
                            buffer.add(bytes_read as usize),
                            readbytes as usize
                        );
                    }

                    offset_bytes = 0;
                    bytes_read += readbytes;
                    bytes_rem -= readbytes;

                    if bytes_rem == 0 {
                        return bytes_read;
                    }
                }

                blocks_seen += 1;
            }
        }

        ///////////////////////////////////////////////
        ///// DOUBLE INDIRECT ZONES
        ///////////////////////////////////////////////
        if inode.zones[8] != 0 {
            syscall_block_read(
                desc.blkdev,
                i1direct_buffer.buffer,
                MINIX3_BLOCK_SIZE,
                inode.zones[8] * MINIX3_BLOCK_SIZE
            );

            for i in 0..num_indirect_ptr {
                let i1zone_offset = unsafe {
                    i1zones.add(i).read()
                };

                if i1zone_offset == 0 { continue; }

                syscall_block_read(
                    desc.blkdev,
                    i2direct_buffer.buffer,
                    MINIX3_BLOCK_SIZE,
                    i1zone_offset * MINIX3_BLOCK_SIZE
                );

                for j in 0..num_indirect_ptr {
                    let i2zone_offset = unsafe {
                        i2zones.add(i).read()
                    };

                    if i2zone_offset == 0 { continue; }

                    if offset_block <= blocks_seen {
                        syscall_block_read(
                            desc.blkdev,
                            block_buffer.buffer,
                            MINIX3_BLOCK_SIZE,
                            i2zone_offset * MINIX3_BLOCK_SIZE
                        );

                        let readbytes
                        = if MINIX3_BLOCK_SIZE - offset_bytes > bytes_rem {
                            bytes_rem
                        }
                        else {
                            MINIX3_BLOCK_SIZE - bytes_rem
                        };

                        unsafe {
                            ptr::copy_nonoverlapping(
                                block_buffer.buffer.add(offset_bytes as usize),
                                buffer.add(bytes_read as usize),
                                readbytes as usize
                            );
                        }

                        offset_bytes = 0;
                        bytes_read += readbytes;
                        bytes_rem -= readbytes;

                        if bytes_rem == 0 {
                            return bytes_read;
                        }
                    }

                    blocks_seen += 1;
                }
            }
        }

        ///////////////////////////////////////////////
        ///// TRIPLY INDIRECT ZONES
        ///////////////////////////////////////////////
        if inode.zones[9] != 0 {
            syscall_block_read(
                desc.blkdev,
                i1direct_buffer.buffer,
                MINIX3_BLOCK_SIZE,
                inode.zones[9] * MINIX3_BLOCK_SIZE
            );

            for i in 0..num_indirect_ptr {
                let i1zone_offset = unsafe {
                    i1zones.add(i).read()
                };

                if i1zone_offset == 0 { continue; }

                syscall_block_read(
                    desc.blkdev,
                    i2direct_buffer.buffer,
                    MINIX3_BLOCK_SIZE,
                    i1zone_offset * MINIX3_BLOCK_SIZE
                );

                for j in 0..num_indirect_ptr {
                    let i2zone_offset = unsafe {
                        i2zones.add(i).read()
                    };

                    if i2zone_offset == 0 { continue; }

                    syscall_block_read(
                        desc.blkdev,
                        i3direct_buffer.buffer,
                        MINIX3_BLOCK_SIZE,
                        i2zone_offset * MINIX3_BLOCK_SIZE
                    );

                    for k in 0..num_indirect_ptr {
                        let i3zone_offset = unsafe {
                            i3zones.add(i).read()
                        };

                        if i3zone_offset == 0 { continue; }

                        if offset_block <= blocks_seen {
                            syscall_block_read(
                                desc.blkdev,
                                block_buffer.buffer,
                                MINIX3_BLOCK_SIZE,
                                i3zone_offset * MINIX3_BLOCK_SIZE
                            );

                            let readbytes
                            = if MINIX3_BLOCK_SIZE - offset_bytes > bytes_rem {
                                bytes_rem
                            }
                            else {
                                MINIX3_BLOCK_SIZE - bytes_rem
                            };

                            unsafe {
                                ptr::copy_nonoverlapping(
                                    block_buffer.buffer.add(offset_bytes as usize),
                                    buffer.add(bytes_read as usize),
                                    readbytes as usize
                                );
                            }

                            offset_bytes = 0;
                            bytes_read += readbytes;
                            bytes_rem -= readbytes;

                            if bytes_rem == 0 {
                                return bytes_read;
                            }
                        }

                        blocks_seen += 1;
                    }
                }
            }
        }

        bytes_read
    }

    fn write(desc: &FileDesc, buffer: *mut u8, size: u32, offset: u32) -> u32 {
        todo!()
    }

    fn close(desc: &FileDesc) {
        todo!()
    }

    fn stats(desc: &FileDesc) -> fs::Stats {
        let inode = Self::get_inode(desc, desc.node).unwrap();
        unsafe {
            Stats {
                mode: (*inode).mode,
                size: (*inode).size,
                uid: (*inode).uid,
                gid: (*inode).gid
            }
        }
    }
}


///////////////////////////////////////////////////////////////////////////////
//// Proccess Args
struct FsProcArgs {
    pub pid: pid_t,
    pub dev: usize,
    pub buffer: *mut u8,
    pub size: u32,
    pub offset: u32,
    pub node: u32
}

fn read_rountine(args_addr: usize) {
    let args_ptr = args_addr as *mut FsProcArgs;
    let args = unsafe{ args_ptr.as_ref().unwrap() };

    let desc = FileDesc {
        blkdev: args.dev,
        node: args.node,
        loc: 0,
        size: 500,
        pid: args.pid
    };

    let bytes = Minix3Fs::read(&desc, args.buffer, args.size, args.offset);

    unsafe {
        if let Some(proc) = get_proc(args.pid) {
            (*proc.get_frame_mut()).regs[10] = bytes as usize;
        }
    }

    set_running(args.pid);

    kfree(args_ptr as *mut u8);
}

pub fn new_read_process(
    pid: pid_t,
    dev: usize,
    node: u32,
    buffer: *mut u8,
    size: u32,
    offset: u32,
)
{
    let mut args = talloc::<FsProcArgs>().unwrap();

    args.pid = pid;
    args.dev = dev;
    args.buffer = buffer;
    args.size = size;
    args.offset = offset;
    args.node = node;

    set_waiting(pid);
    Process::new_kernel_proc_args(read_rountine, args as *mut FsProcArgs as usize);
}

