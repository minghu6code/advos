//! Trap routines

use core::mem::transmute;

use m6binpack::{ unpack, Unpack };

use crate::cpu::TrapFrame;
use crate::proc::rm_proc;
use crate::sched::schedule;
use crate::syscall::do_syscall;
use crate::*;
use crate::plic;
use crate::uart::{
    UART
};


#[no_mangle]
extern "C" fn m_trap(
    epc: usize,
    tval: usize,  // trap value
    cause: usize,
    hartid: usize,
    status: usize,
    frame: *mut TrapFrame,
) -> usize {
    unpack! {
        <causeid: usize: 63><is_async: bool: 1> = cause;
    };

    let mut ra = epc;

    // Asynchronous trap (outside trap),
    if is_async {
        match causeid {
            3 => {
                println!("Machine software interrupt CPU#{}", hartid);
            },
            7 => {  // Machine timer interrupt
                    // This is the context-switch timer (time-slicing).
                    sched_nxt_ctx_sw(1);
                    rust_switch_to_user(schedule());
            },
            11 => {
                // Machine external (interrupt from Platform Interrupt Controller (PLIC))
                plic::handle_interrupt();
            },
            _ => {
                panic!("Unhandled async trap CPU#{} -> {}\n", hartid, causeid);
            }
        }
    }
    // Synchronous trap (inside trap)
    else {
        match causeid {
            2 => { // Illegal instruction
                rm_proc(unsafe { (*frame).pid } as pid_t);
            },
            8 | 9 | 11 => unsafe {
                // Environment (system) call
                // println!("E-call CPU#{} -> 0x{:08x}", hartid, epc);
                ra = do_syscall(epc, frame);

                if ra == 0 {
                    (*frame).pc += 4;
                    sched_nxt_ctx_sw(1);
                    rust_switch_to_user(schedule());
                }
            },
            // Page faults
            12 => {
                // Instruction page fault
                println!(
                    "Instruction page fault CPU#{} -> 0x{:08x}: 0x{:08x}",
                    hartid, epc, tval
                );
                rm_proc(unsafe { (*frame).pid } as pid_t);
                sched_nxt_ctx_sw(1);
                rust_switch_to_user(schedule());
            },
            13 => {
                // Load page fault
                println!(
                    "Load page fault CPU#{} -> 0x{:08x}: 0x{:08x}",
                    hartid, epc, tval
                );
                rm_proc(unsafe { (*frame).pid } as pid_t);
                sched_nxt_ctx_sw(1);
                rust_switch_to_user(schedule());
            },
            15 => {
                // Store page fault
                println!(
                    "Store page fault CPU#{} -> 0x{:08x}: 0x{:08x}",
                    hartid, epc, tval
                );
                rm_proc(unsafe { (*frame).pid } as pid_t);
                sched_nxt_ctx_sw(1);
                rust_switch_to_user(schedule());
            },
            _ => {
                panic!("Unhandled sync trap CPU#{} -> {}\n", hartid, causeid);
            }
        }
    }

    ra
}

/// m: multiplier
pub fn sched_nxt_ctx_sw(m: u16) {
    unsafe {
        (MTIMECMP as *mut u64).write_volatile(
            (MTIME as *mut u64).read_volatile()
            .wrapping_add(CONTEXT_SWITCH_TIME * m as u64)  // 取摸加法
        );
    }
}
