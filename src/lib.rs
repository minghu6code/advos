#![allow(unused)]
#![allow(non_camel_case_types)]
#![allow(internal_features)]
#![allow(static_mut_refs)]
#![feature(trait_alias)]

#![no_std] // don't link the Rust standard library
// #![no_main] // disable all Rust-level entry points

#![feature(alloc_error_handler)]
#![feature(type_alias_impl_trait)]
#![feature(lang_items)]
#![feature(const_trait_impl)]


pub(crate) mod stdalt;
pub(crate) mod uart;
pub(crate) mod page;
pub(crate) mod utils;
pub(crate) mod kmem;
pub(crate) mod cpu;
pub(crate) mod trap;
pub(crate) mod r#const;
pub(crate) mod plic;
pub(crate) mod proc;
pub(crate) mod typedef;
pub(crate) mod syscall;
pub(crate) mod sched;
pub(crate) mod device;
pub(crate) mod fs;
pub(crate) mod io;


#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate alloc;

use core::arch::asm;
use core::panic::PanicInfo;
use core::mem::size_of;
use alloc::boxed::Box;
use alloc::string::String;

pub use m6binpack:: {
    unpack
};

use crate::device::block;
use crate::page::{
    EntrySpec, Page, PageTable,
    ALLOC_START, ALLOC_END, HDR_PAGE_END,
    vir2phy, zalloc
};
use crate::trap::sched_nxt_ctx_sw;
use crate::utils::{
    align4k, round_up_align4k
};
use crate::kmem::{MEM_K_END, MEM_K_HEAD, MEM_K_PT, kfree};
use crate::cpu::{
    build_satp, SatpMode, TrapFrame,
    mscratch_write, sscratch_write, mscratch_read,
    satp_write, satp_fence_asid
};
pub use crate::r#const::*;
pub use crate::typedef::*;
use crate::uart::UART;
use crate::proc::{

};
use io::virtio;


#[lang="eh_personality"] extern fn eh_personality() {}

/// This function is called on panic.
#[panic_handler]
fn handle_panic(info: &PanicInfo) -> ! {
    print!("Aborting: ");

    if let Some(p) = info.location() {
        println!(
            "line {}, file {}: {}",
            p.line(),
            p.file(),
            info.message()
        );
    } else {
        println!("no information available");
    }

    abort();
}

#[no_mangle]
extern "C" fn abort() ->! {
    loop {
        unsafe {
            asm!("wfi")
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//// Foreign Symbols

extern "C" {
    static TEXT_START: usize;
    static TEXT_END: usize;
    static DATA_START: usize;
    static DATA_END: usize;
    static RODATA_START: usize;
    static RODATA_END: usize;
    static BSS_START: usize;
    static BSS_END: usize;
    static STACK_START: usize;
    static STACK_END: usize;
    static HEAP_START: usize;
    static HEAP_END: usize;

    fn switch_to_user(frame: usize) -> !;
}

pub fn rust_switch_to_user(frame: usize) -> ! {
    unsafe {
        switch_to_user(frame)
    }
}

/// This assumes that start <= end
pub fn id_map_range(
    root_table: &mut PageTable,
    start: usize,
    end: usize,
    entry_spec: EntrySpec
)
{
    let mut addr = align4k(start);
    let kb_level_pages_num = (round_up_align4k(end) - addr) / PAGE_SIZE;

    for _ in 0..kb_level_pages_num {
        page::map(root_table, addr, addr, entry_spec, 0);
        addr += 1 << 12;  // 4K
    }
}

///////////////////////////////////////////////////////////////////////////////
//// ENTRY POINT

#[no_mangle]
pub extern "C" fn kernel_init() {
    uart::init_uart();
    page::init_page();
    kmem::init_km();
    proc::init_proc();

    println!();
    println!("This is Adv OS!");

    plic::set_threshold(0);
    // VIRTIO = [1..8]
    // UART0 = 10
    // PCIE = [32..35]
    // Enable PLIC interrupts.
    for i in 1..=10 {
        plic::enable(i);
        plic::set_priority(i, 1);
    }

    virtio::probe_device();

    #[cfg(feature = "customtest")]
    {
        println!("start customtest: ...");
        customtest();
    }

    #[cfg(not(feature = "customtest"))]
    {
        use crate::device::block::customtest::test_block;

        proc::Process::new_kernel_proc(test_block);
        sched_nxt_ctx_sw(1);
        rust_switch_to_user(sched::schedule());
    }
}

#[no_mangle]
extern "C" fn kernel_init_hart(hartid: usize) {
    // All non-zero harts initialize here.
}

#[cfg(feature = "customtest")]
fn customtest() {
    use proc::Process;
    use proc::customtest::test_pid_bitmap;

    use crate::proc::customtest::*;
    use crate::device::rng::customtest::*;
    use crate::device::block::customtest::*;

    test_pid_bitmap();

    // // Test Block
    // Process::new_kernel_proc(test_block);
    // sched_nxt_ctx_sw(1);
    // rust_switch_to_user(sched::schedule());
    // test_random();
}
