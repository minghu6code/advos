//! CPU and CPU-related routines,
//! Also contains the kernel's trap frame

use core::arch::asm;

use crate::*;

///////////////////////////////////////////////////////////////////////////////
//// SATP

#[repr(usize)]
#[derive(Clone, Copy)]
pub enum SatpMode {
    Off = 0,
    Sv39 = 8,
    Sv48 = 9,
}

impl From<usize> for SatpMode {
    fn from(val: usize) -> Self {
        match val {
            0 => SatpMode::Off,
            8 => SatpMode::Sv39,
            9 => SatpMode::Sv48,
            _ => unreachable!()
        }
    }
}

/// asid: AddressSpaceID: 地址空间描述符
pub const fn build_satp(mode: SatpMode, asid: usize, addr: usize) -> usize {
    (mode as usize) << 60 | (asid & 0xffff) << 44 | (addr >> 12) & 0xff_ffff_ffff
}


///////////////////////////////////////////////////////////////////////////////
//// CPUMode

#[repr(usize)]
pub enum CPUMode {
    U = 0,
    S = 1,
    M = 2
}


///////////////////////////////////////////////////////////////////////////////
//// TrapFrame

#[derive(Clone, Copy)]
// repr(C) 使用C struct的memory layout
#[repr(C)]
pub struct TrapFrame {      // unit: bytes
    pub regs: [usize; 32],  // 0 - 255   | 通用寄存器: x0-x31
    pub fregs: [usize; 32], // 256 - 511 | 浮点数寄存器: f0-f31
    pub satp: usize,        // 512 - 519 | satp register
    pub pc: usize,          // 520
    pub hartid: usize,      // 528
    pub m: usize,           // 536
    pub pid: usize,         // 544
    pub mode: usize,        // 552
}

impl TrapFrame {
    pub const fn zero() -> Self {
        Self {
            regs: [0; 32],
            fregs: [0; 32],
            satp: 0,
            pc: 0,
            hartid: 0,
            m: 1,
            pid: 0,
            mode: 0
        }
    }
}


///////////////////////////////////////////////////////////////////////////////
//// ASM Wrapper

pub fn mhartid_read() -> usize {
    unsafe {
        let rval;
        asm!("csrr {0}, mhartid", out(reg) rval);
        rval
    }
}

pub fn mstatus_write(val: usize) {
    unsafe {
        asm!("csrw	mstatus, {0}", in(reg) val);
    }
}

pub fn mstatus_read() -> usize {
    unsafe {
        let rval;
        asm!("csrr {0}, mstatus", out(reg) rval);
        rval
    }
}

pub fn stvec_write(val: usize) {
    unsafe {
        asm!("csrw stvec, {0}", in(reg) val);
    }
}

pub fn stvec_read() -> usize {
    unsafe {
        let rval;
        asm!("csrr {0}, stvec", out(reg) rval);
        rval
    }
}

pub fn mscratch_write(val: usize) {
    unsafe {
        asm!("csrw	mscratch, {0}", in(reg) val);
    }
}

pub fn mscratch_read() -> usize {
    unsafe {
        let rval;
        asm!("csrr	{0}, mscratch", out(reg) rval);
        rval
    }
}

pub fn mscratch_swap(to: usize) -> usize {
    unsafe {
        let from;
        asm!("csrrw {0}, mscratch, {1}", out(reg) from, in(reg) to);
        from
    }
}

pub fn sscratch_write(val: usize) {
    unsafe {
        asm!("csrw	sscratch, {0}", in(reg) val);
    }
}

pub fn sscratch_read() -> usize {
    unsafe {
        let rval;
        asm!("csrr {0}, sscratch", out(reg) rval);
        rval
    }
}

pub fn sscratch_swap(to: usize) -> usize {
    unsafe {
        let from;
        asm!("csrrw {0}, sscratch, {1}", out(reg) from, in(reg) to);
        from
    }
}

pub fn sepc_write(val: usize) {
    unsafe {
        asm!("csrw sepc, {0}", in(reg) val);
    }
}

pub fn sepc_read() -> usize {
    unsafe {
        let rval;
        asm!("csrr {0}, sepc", out(reg) rval);
        rval
    }
}

pub fn satp_write(val: usize) {
    unsafe {
        asm!("csrw satp, {0}", in(reg) val);
    }
}

pub fn satp_read() -> usize {
    unsafe {
        let rval;
        asm!("csrr {0}, satp", out(reg) rval);
        rval
    }
}

pub fn satp_fence(vaddr: usize, asid: usize) {
    unsafe {
        asm!("sfence.vma {0}, {1}", in(reg) vaddr, in(reg) asid);
    }
}

pub fn satp_fence_asid(asid: usize) {
    unsafe {
        asm!("sfence.vma zero, {0}", in(reg) asid);
    }
}

pub fn get_mtime() -> usize {
    unsafe { *(MTIME as *const usize) }
}

///////////////////////////////////////////////////////////////////////////////
//// Helper Tools

pub fn dump_register(frame: *const TrapFrame) {
    print!("   ");
    for i in 1..32 {
        if i % 4 == 0 {
            println!();
            print!("   ");
        }
        print!("x{:2}:{:08x}   ", i, unsafe { (*frame).regs[i] });
    }
    println!();
}
