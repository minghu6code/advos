use crate::*;
use crate::kmem::kmalloc;
use crate::virtio::{DevDesc, MMIOSpec, MMIOStatus, VirtQueue};

///////////////////////////////////////////////////////////////////////////////
//// Entropy Device Data Structure

#[derive(Clone, Copy)]
pub struct EntropyDevice {
    queue: *mut VirtQueue,
    dev: *mut u32,
    idx: u16,
    ack_used_idx: u16,
}

static mut ENTROPY_DEVICE:[Option<EntropyDevice>; 8] = [None; 8];

/// Totally Be same with block device setup, except features
pub fn setup_entropy_device(ptr: *mut u32) -> bool {
    unsafe {
        let idx = get_device_id(ptr);

        // 1. Reset the device (write 0 into status)
        ptr.add(MMIOSpec::Status.scale32()).write_volatile(0);

        // 2. Set ACKNOWLEGE status bit
        let mut status_bits = MMIOStatus::Acknowledge as u32;
        ptr.add(MMIOSpec::Status.scale32())
            .write_volatile(status_bits);

        // 3. Set the DRIVER status bit
        status_bits |= MMIOStatus::DriverOk as u32;
        ptr.add(MMIOSpec::Status.scale32())
            .write_volatile(status_bits);

        // 4. Read device feature bits, write anything from host features but ro
        let host_features = ptr.add(MMIOSpec::HostFeatures.scale32()).read_volatile();
        let guest_features = host_features;
        ptr.add(MMIOSpec::GuestFeatures.scale32())
            .write_volatile(guest_features);

        // 5. Set FEATURES_OK status bit
        status_bits |= MMIOStatus::FeaturesOk as u32;
        ptr.add(MMIOSpec::Status.scale32())
            .write_volatile(status_bits);

        // 6. Re-read status to ensure FEATURES_OK is still set.
        // otherwise, it doesn't support our features.
        let status_ok = ptr.add(MMIOSpec::Status.scale32()).read_volatile();
        if MMIOStatus::FeaturesOk as u32 & status_ok == 0 {
            print!("features fail...");

            ptr.add(MMIOSpec::Status.scale32())
                .write_volatile(MMIOStatus::Failed as u32);

            return false;
        }

        // 7. Perform device-specific setup.
        let qnmax = ptr.add(MMIOSpec::QueueNumMax.scale32()).read_volatile();
        if VIRTIO_RING_SIZE as u32 > qnmax {
            print!("{} great than QueueNumMax: {}...", qnmax, VIRTIO_RING_SIZE);
            return false;
        }
        ptr.add(MMIOSpec::QueueNum.scale32())
            .write_volatile(VIRTIO_RING_SIZE as u32);

        ptr.add(MMIOSpec::QueueSel.scale32()).write_volatile(0);

        let pages: usize = round_up_div!(size_of::<VirtQueue>(), PAGE_SIZE);
        let queue_ptr = zalloc(pages).unwrap() as *mut VirtQueue;
        ptr.add(MMIOSpec::GuestPageSize.scale32()).write_volatile(PAGE_SIZE as u32);

        // QueuePFN is a physical page number,
        // however it appears for QEMU we have to write the entire memory address.
        ptr.add(MMIOSpec::QueuePFN.scale32())
            .write_volatile(queue_ptr as u32 / PAGE_SIZE as u32);

        // 8. Set the DRIVER_OK status bit.
        // Device is now "live"
        status_bits |= MMIOStatus::DriverOk as u32;
        ptr.add(MMIOSpec::Status.scale32())
            .write_volatile(status_bits);

        ENTROPY_DEVICE[idx] = Some(
            EntropyDevice {
                queue: queue_ptr,
                dev: ptr,
                idx: 0,
                ack_used_idx: 0
            }
        )
    }

    true
}


pub fn random64() -> u64 {
    unsafe {
        for devopt in ENTROPY_DEVICE.iter() {
            if let Some(dev) = devopt {
                let ptr = kmalloc(8).unwrap();
                let _desc = DevDesc {
                    addr: ptr as u64,
                    len: 8,
                    flags: VIRTIO_DESC_F_WRITE,
                    next: 0
                };
                let _val = *ptr as u64;
                kfree(ptr);
                break;
            }
        }
    }

    0u64.wrapping_sub(1)
}

///////////////////////////////////////////////////////////////////////////////
//// Test
#[cfg(feature = "customtest")]
pub mod customtest {
    use super::*;
    use crate::*;

    pub fn test_random() {
        for i in 0..10 {
            println!("{}", random64());
        }
    }
}
