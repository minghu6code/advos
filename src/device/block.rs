use core::{mem::size_of, ops::Add};

use crate::{*, kmem::{kfree, kmalloc, talloc}, page::{zalloc, Page}, proc::{Process, get_proc, set_running, set_waiting}, round_up_div, virtio::{DevDesc, MMIOSpec, MMIOStatus, VirtQueue}};

///////////////////////////////////////////////////////////////////////////////
//// Block Device Data Structure

#[derive(Clone, Copy)]
pub struct BlockDevice {
    queue: *mut VirtQueue,
    dev: *mut u32,
    idx: u16,
    ack_used_idx: u16,
    is_ro: bool,
}

/// 16 bytes align
#[derive(Clone, Copy)]
#[repr(C)]
pub struct BlkHeader {
    blk_t: u32,
    padding: u32,
    sector: u64,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct BlkData {
    data: *mut u8,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct BlkStatus {
    status: u8,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct BlkReq {
    hdr: BlkHeader,
    data: BlkData,
    status: BlkStatus,
    head: u16,

    // This is the PID of watcher,
    // because it's possible that process dies before we get here.
    // If we used a pointer, we may derefrence invalid memory.
    watcher: pid_t
}

///////////////////////////////////////////////////////////////////////////////
//// Block Device Setup

static mut BLOCK_DEVICES: [Option<BlockDevice>; 8] = [None; 8];

pub fn setup_block_device(ptr: *mut u32) -> bool {
    unsafe {
        // 0x1000_1000: index 0
        // 0x1000_2000: index 1
        // 0x1000_3000: index 2
        let idx = get_device_id(ptr);

        // 1. Reset the device (write 0 into status)
        ptr.add(MMIOSpec::Status.scale32()).write_volatile(0);

        // 2. Set ACKNOWLEGE status bit
        let mut status_bits = MMIOStatus::Acknowledge as u32;
        ptr.add(MMIOSpec::Status.scale32())
            .write_volatile(status_bits);

        // 3. Set the DRIVER status bit
        status_bits |= MMIOStatus::DriverOk as u32;
        ptr.add(MMIOSpec::Status.scale32())
            .write_volatile(status_bits);

        // 4. Read device feature bits, write anything from host features but ro
        let host_features = ptr.add(MMIOSpec::HostFeatures.scale32()).read_volatile();
        let guest_features = host_features & !(1 << VIRTIO_BLK_F_RO);
        ptr.add(MMIOSpec::GuestFeatures.scale32())
            .write_volatile(guest_features);

        // 5. Set FEATURES_OK status bit
        status_bits |= MMIOStatus::FeaturesOk as u32;
        ptr.add(MMIOSpec::Status.scale32())
            .write_volatile(status_bits);

        // 6. Re-read status to ensure FEATURES_OK is still set.
        // otherwise, it doesn't support our features.
        let status_ok = ptr.add(MMIOSpec::Status.scale32()).read_volatile();
        if MMIOStatus::FeaturesOk as u32 & status_ok == 0 {
            print!("features fail...");

            ptr.add(MMIOSpec::Status.scale32())
                .write_volatile(MMIOStatus::Failed as u32);

            return false;
        }

        // 7. Perform device-specific setup.
        // Set the queue num.
        // We have to make sure that the queue size is valid
        // because the device can only take a certain size.
        let qnmax = ptr.add(MMIOSpec::QueueNumMax.scale32()).read_volatile();
        if VIRTIO_RING_SIZE as u32 > qnmax {
            print!("{} great than QueueNumMax: {}...", qnmax, VIRTIO_RING_SIZE);
            return false;
        }
        ptr.add(MMIOSpec::QueueNum.scale32())
            .write_volatile(VIRTIO_RING_SIZE as u32);

        // We allocate a page for each device.
        // This will the the descriptor where we can communicate with the block device.
        // We will still use an MMIO register
        // (in particular, QueueNotify) to actually tell the device we put something in memory.
        // We also have to be careful with memory ordering.
        // We don't want to issue a notify before all memory writes have finished.
        // We will look at that later, but we need what is called a memory "fence" or barrier.
        ptr.add(MMIOSpec::QueueSel.scale32()).write_volatile(0);

        let pages: usize = round_up_div!(size_of::<VirtQueue>(), PAGE_SIZE);
        let queue_ptr = zalloc(pages).unwrap() as *mut VirtQueue;
        ptr.add(MMIOSpec::GuestPageSize.scale32()).write_volatile(PAGE_SIZE as u32);

        // QueuePFN is a physical page number,
        // however it appears for QEMU we have to write the entire memory address.
        ptr.add(MMIOSpec::QueuePFN.scale32())
            .write_volatile(queue_ptr as u32 / PAGE_SIZE as u32);
        let blkdev = BlockDevice {
            queue: queue_ptr,
            dev: ptr,
            idx: 0,
            ack_used_idx: 0,
            is_ro: host_features & (1 << VIRTIO_BLK_F_RO) != 0,
        };
        BLOCK_DEVICES[idx] = Some(blkdev);

        // 8. Set the DRIVER_OK status bit.
        // Device is now "live"
        status_bits |= MMIOStatus::DriverOk as u32;
        ptr.add(MMIOSpec::Status.scale32())
            .write_volatile(status_bits);
    }

    true
}

///////////////////////////////////////////////////////////////////////////////
//// Block Device Operation

#[derive(PartialEq, Eq)]
pub enum BlkIO {
    W,
    R,
}

/// size should be multiplier of 512
pub fn block_io(
    dev: usize,
    buffer: *mut u8,
    size: u32,
    offset: u64,
    iotype: BlkIO,
    watcher: pid_t
)
-> Result<(), TrapError>
{
    unsafe {
        if let Some(device) = BLOCK_DEVICES[dev - 1].as_mut() {
            if device.is_ro && iotype == BlkIO::W {
                return Err(TrapError {
                    code: TrapCode::WriteToReadOnly,
                    msg: String::new()
                });
            }

            if size % SECTOR_SIZE as u32 != 0 {
                return Err(TrapError {
                    code: TrapCode::InvalidArgument,
                    msg: format!("size: {} must be multiplier of {}", size, SECTOR_SIZE)
                });
            }

            // 扇区
            let sector = offset / 512;
            let req = kmalloc(size_of::<BlkReq>()).unwrap() as *mut BlkReq;
            let desc = DevDesc {
                addr: &(*req).hdr as *const BlkHeader as u64,
                len: size_of::<BlkHeader>() as u32,
                flags: VIRTIO_DESC_F_NEXT,
                next: 0,
            };
            let head_idx = fill_next_devdesc(device, desc);
            (*req).hdr.sector = sector;
            (*req).hdr.blk_t = if iotype == BlkIO::W {
                VIRTIO_BLK_T_OUT
            } else {
                VIRTIO_BLK_T_IN
            };

            (*req).data.data = buffer;
            // We put 111 in the status. Whenever the device finishes,
            // it will write into status.
            // If we read status and it is 111,
            // we know that it wasn't written to by the device?
            (*req).status.status = 111;
            (*req).watcher = watcher;
            fill_next_devdesc(
                device,
                DevDesc {
                    addr: buffer as u64,
                    len: size,
                    flags: VIRTIO_DESC_F_NEXT
                        | if iotype == BlkIO::R {
                            VIRTIO_DESC_F_WRITE
                        } else {
                            0
                        },
                    next: 0,
                },
            );
            fill_next_devdesc(
                device,
                DevDesc {
                    addr: &(*req).status as *const BlkStatus as u64,
                    len: size_of::<BlkStatus>() as u32,
                    flags: VIRTIO_DESC_F_WRITE,
                    next: 0
                }
            );

            (*device.queue).avaring.ring[(*device.queue).avaring.idx as usize]
            = head_idx;
            (*device.queue).avaring.idx
            = ((*device.queue).avaring.idx + 1) % VIRTIO_RING_SIZE as u16;

            // block device has just one queue (0), which is request queue
            device.dev.add(MMIOSpec::QueueNotify.scale32()).write_volatile(0);
        }
    }

    Ok(())
}

pub fn fill_next_devdesc(device: &mut BlockDevice, mut desc: DevDesc) -> u16 {
    unsafe {
        device.idx = (device.idx + 1) % VIRTIO_RING_SIZE as u16;

        if desc.flags & VIRTIO_DESC_F_NEXT != 0 {
            desc.next = (device.idx + 1) % VIRTIO_RING_SIZE as u16;
        }

        (*device.queue).descvec[device.idx as usize] = desc;

        device.idx
    }
}

pub fn read(dev: usize, buffer: *mut u8, size: u32, offset: u64)
-> Result<(), TrapError>
{
    block_io(dev, buffer, size, offset, BlkIO::R, 0)
}

pub fn write(dev: usize, buffer: *mut u8, size: u32, offset: u64)
-> Result<(), TrapError>
{
    block_io(dev, buffer, size, offset, BlkIO::W, 0)
}

///////////////////////////////////////////////////////////////////////////////
//// Block Device Request / Response

/// Handle block specific interrupts.
/// We need to check the used ring and wind it up until everything handled.
pub fn pending(blkdev: &mut BlockDevice) {
    unsafe {
        let ref queue = *blkdev.queue;
        while blkdev.ack_used_idx != queue.usedring.idx {
            let ref elem
            = queue.usedring.ring[blkdev.ack_used_idx as usize];

            blkdev.ack_used_idx = blkdev.ack_used_idx.wrapping_add(1);
            let req = queue.descvec[elem.id as usize].addr as *const BlkReq;

            if (*req).watcher > 0 {
                set_running((*req).watcher);
                let mut proc = get_proc((*req).watcher).unwrap();
                (*proc.get_frame_mut()).regs[10] = (*req).status.status as usize;
            }

            kfree(req as *mut u8);
        }
    }
}

pub fn handle_interrupt(idx: usize) {
    unsafe {
        if let Some(blkdev) = BLOCK_DEVICES[idx].as_mut() {
            pending(blkdev);
        }
        else {
            println!("Invalid block device for interrupt {}", idx + 1);
        }
    }
}


///////////////////////////////////////////////////////////////////////////////
//// Test
//#[cfg(feature = "customtest")]
pub mod customtest {
    use core::ptr;

    use super::*;
    use crate::{*, cpu::CPUMode, page::{PAGE_NUM, map}, proc::{gen_pid, insert_proc}, syscall::syscall_fs_read};

    pub fn test_block() {
        // println!("Test Block");

        let bytes_to_read = 50 * MINIX3_BLOCK_SIZE;
        let buffer = kmalloc(bytes_to_read as usize).unwrap();
        let bytes_read = syscall_fs_read(8, 8, buffer, bytes_to_read, 0);

        let mut proc = Process::new_default();
        let prog_pages = round_up_div!(bytes_read, PAGE_SIZE);
        proc.prog = zalloc(prog_pages).unwrap();

        print!("hiihihihi");

        unsafe {
            ptr::copy_nonoverlapping(buffer, proc.prog, bytes_read);
        }

        for i in 0..prog_pages {
            let vir_addr = PROC_STARTING_ADDR + i * PAGE_SIZE;
            let phy_addr = proc.prog as usize + i * PAGE_SIZE;

            page::map(
                unsafe { &mut *(proc.table) },
                vir_addr,
                phy_addr,
                EntrySpec::URWX,
                0
            );
        }

        for i in 0..PROC_STACK_PAGES {
            let vir_addr = PROC_STACK_ADDR + i * PAGE_SIZE;
            let phy_addr = proc.stack as usize + i * PAGE_SIZE;

            page::map(
                unsafe { &mut *(proc.table) },
                vir_addr,
                phy_addr,
                EntrySpec::URW,
                0
            );
        }

        unsafe {
            // The pc is a virtual memory address and is loaded into mepc.
            (*proc.frame).pc = PROC_STARTING_ADDR;
            (*proc.frame).regs[2] = PROC_STACK_ADDR + PROC_STACK_PAGES * PAGE_SIZE;
            (*proc.frame).mode = CPUMode::U as usize;
            (*proc.frame).pid = proc.pid as usize;
            // The SATP register is used for the MMU,
            // so we need to map our table into register.
            // The switch_to_user function will load satp into the actual register
            // when the time comes.
            (*proc.frame).satp
            = build_satp(
                SatpMode::Sv39,
                proc.pid as usize,
                proc.table as usize
            );
        }

        satp_fence_asid(proc.pid as usize);

        insert_proc(proc);
        println!(
            "Added user process to the scheduler...get \
             ready for take-off!"
        );

        kfree(buffer);
    }
}
