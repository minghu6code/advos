pub(crate) mod block;
pub(crate) mod rng;

pub use block::setup_block_device;
pub use rng::setup_entropy_device;

///////////////////////////////////////////////////////////////////////////////
//// device setup

// The procedures for configurating a device are laid out in the specification as:
// 1. Reset the device by writing 0 to the status register.
// 2. Set the ACKNOWLEDGE status bit to the status register.
// 3. Set the DRIVER status bit to the status register.
// 4. Read device features from host_features register.
// 5. Negotiate the set of features and write what you'll accept to guest_features register.
// 6. Set the FEATURES_OK status bit to the status register.
// 7. Re-read the status register to confirm that the device accepted your features.
// 8. Perform device-specific setup.
// 9. Set the DRIVER_OK status bit to the status register. The device is now LIVE.



pub fn setup_network_device(_ptr: *mut u32) -> bool {
    false
}

pub fn setup_gpu_device(_ptr: *mut u32) -> bool {
    false
}

pub fn setup_input_device(_ptr: *mut u32) -> bool {
    false
}

