
use core::{mem::size_of, ptr::null_mut};

use crate::{
    page::{
        PageTable,
        zalloc
    },
    println,
    utils::round_up_align
};
use crate::r#const::*;

// Sub page level: malloc allocation system

#[repr(usize)]
enum AllocListSpec {
    Taken = 1 << 63
}

// MM Model
//    8bytes | required bytes (8 bytes aligned)
// [AllocList|Required Memorry]
pub struct AllocList(usize);  // taken flag + size

impl AllocList {
    pub fn is_taken(&self) -> bool {
        self.0 & AllocListSpec::Taken as usize != 0
    }

    pub fn is_free(&self) -> bool {
        !self.is_taken()
    }

    pub fn set_taken(&mut self) {
        self.0 |= AllocListSpec::Taken as usize
    }

    pub fn set_free(&mut self) {
        self.0 &= !(AllocListSpec::Taken as usize)
    }

    /// 设置大小, 并初始化Taken位
    pub fn set_size(&mut self, size: usize) {
        let is_taken = self.is_taken();
        // 先取消相关位
        self.0 = size & !(AllocListSpec::Taken as usize);
        if is_taken {
            self.0 |= AllocListSpec::Taken as usize;
        }
    }

    pub fn get_size(&self) -> usize {
        // 取消最高位 |i64|
        self.0 & !(AllocListSpec::Taken as usize)
    }
}

pub static mut MEM_K_HEAD: *mut AllocList = null_mut();
pub static mut MEM_K_PAGES: usize = 0;
pub static mut MEM_K_END: *mut AllocList = null_mut();
pub static mut MEM_K_PT: *mut PageTable = null_mut();  // page table




/// Initialize kernel's memory.
/// This is not to be used to allocate memory for user processes.
/// If that's the case, use alloc/dealloc from the page crate.
pub fn init_km() {
    unsafe {
        MEM_K_PAGES = 512;  // 总共512 * 4k = 2MB 内核内存
        MEM_K_HEAD = zalloc(MEM_K_PAGES).unwrap() as *mut AllocList;

        (*MEM_K_HEAD).set_free();
        (*MEM_K_HEAD).set_size(MEM_K_PAGES * PAGE_SIZE);

        MEM_K_END
        = (MEM_K_HEAD as *mut u8).add(MEM_K_PAGES * PAGE_SIZE) as *mut AllocList;
        MEM_K_PT = zalloc(1).unwrap() as *mut PageTable;
    }
}

pub fn kmalloc(bytes: usize) -> Option<*mut u8> {
    unsafe {
        let required_size = round_up_align(bytes, 3) + size_of::<AllocList>();
        let mut head = MEM_K_HEAD;
        let tail= MEM_K_END;

        while head < tail {
            if (*head).is_free() && required_size <= (*head).get_size() {
                let chunk_size = (*head).get_size();
                let rem = chunk_size - required_size;
                (*head).set_taken();

                if rem > size_of::<AllocList>() {
                    let next = (head as *mut u8).add(required_size) as *mut AllocList;

                    // There is space remaining here.
                    (*next).set_free();
                    (*next).set_size(rem);

                    (*head).set_size(required_size);
                }
                else {
                    // If we get here, take the entire chunk
                    (*head).set_size(chunk_size);
                }


                return Some(head.add(1) as *mut u8);
            }
            else {
                // If we get here, what we saw wasn't a free
                // chunk, move on to the next.
                head = (head as *mut u8).add((*head).get_size()) as *mut AllocList;
            }
        }
    }

    // If we get here, we didn't find any free chunks--i.e. there isn't
    // enough memory for this. TODO: Add on-demand page allocation.
    None
}

/// Free a sub page allocation
pub fn kfree(ptr: *mut u8) {
    unsafe {
        if !ptr.is_null() {
            // Get the Alloc List (back sizeof(AllocList))
            let p = (ptr as *mut AllocList).offset(-1);
            (*p).set_free();

            coalesce();
        }
    }
}


/// 重新把所有kernel memory中的块合并
/// Merge smaller chunks into a bigger chunk
/// 合并
fn coalesce() {
    unsafe {
        let mut head = MEM_K_HEAD;
        let tail = MEM_K_END;

        while head < tail {
            let next
            = (head as *mut u8).add((*head).get_size()) as *mut AllocList;

            if (*head).get_size() == 0 {
                // If this happens, then we have a bad heap
                // (double free or something). However, that
                // will cause an infinite loop since the next
                // pointer will never move beyond the current
                // location.
                break;
            }
            else if next >= tail {
                // We calculated the next by using the size
                // given as get_size(), however this could push
                // us past the tail. In that case, the size is
                // wrong, hence we break and stop doing what we
                // need to do.
                break;
            }
            else if (*head).is_free() && (*next).is_free() {
                (*head).set_size(
                    (*head).get_size() + (*next).get_size()
                );
            }

            // If we get here, we might've moved. Recalculate new head
            head = (head as *mut u8).add((*head).get_size())
                as *mut AllocList;
        }
    }
}


/*
* Allocate sub page level allocation based on bytes and zero the memory
*/
pub fn kzmalloc(bytes: usize) -> Option<*mut u8> {
    // 8字节对齐
    let bytes = round_up_align(bytes, 3);

    if let Some(ret) = kmalloc(bytes) {
        for i in 0..bytes {
            unsafe {
                (*ret.add(i)) = 0;
            }
        }

        return Some(ret);
    }

    None
}

/// type alloc
pub fn talloc<T>() -> Option<&'static mut T> {
    unsafe {
        if let Some(mem ) = kzmalloc(size_of::<T>()) {
            (mem as *mut T).as_mut()
        }
        else {
            None
        }
    }
}


/// For debugging purpose, print the mem_k table
pub fn print_table() {
    unsafe {
        let mut head = MEM_K_HEAD;
        let tail = MEM_K_END;

        while head < tail {
            println!(
                "{:p} Length = {:<10} (bytes) Taken = {}",
                head,
                (*head).get_size(),
                (*head).is_taken()
            );
            head = (head as *mut u8).add((*head).get_size())
                as *mut AllocList;
        }
    }
}


///////////////////////////////////////////////////////////////////////////////
//// Global Allocator

// The global allocator allows us to use the data structures
// in the core library, such as a linked list or B-tree.
// We want to use these sparingly since we have a coarse-grained
// allocator.
use core::alloc::{GlobalAlloc, Layout};

// The global allocator is a static constant to a global allocator
// structure. We don't need any members because we're using this
// structure just to implement alloc and dealloc.
struct OSGlobalAlloc;

unsafe impl GlobalAlloc for OSGlobalAlloc {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {

        kzmalloc(layout.size()).unwrap()
    }

    unsafe fn dealloc(&self, ptr: *mut u8, _layout: Layout) {
        kfree(ptr)
    }
}

#[global_allocator]
static GA: OSGlobalAlloc = OSGlobalAlloc {};

#[alloc_error_handler]
/// If for some reason alloc() in the global allocator gets null_mut(),
/// then we come here. This is a divergent function, so we call panic to
/// let the tester know what's going on.
pub fn alloc_error(l: Layout) -> ! {
    panic!(
            "Allocator failed to allocate {} bytes with {}-byte alignment.",
            l.size(),
            l.align()
    );
}
