#![allow(unused)]


use core::mem::size_of;
use core::ops::{ Add, BitOr, BitOrAssign };
use core::ptr::null_mut;

use crate::{ print, println };
use crate::utils::*;
use crate::*;

extern "C" {
    static HEAP_START: usize;
    static HEAP_END: usize;
    static HEAP_SIZE: usize;
}

pub static mut PAGE_NUM: usize = 0;
pub static mut HDR_PAGE_END: usize = 0;
pub static mut ALLOC_START: usize = 0;
pub static mut ALLOC_END: usize = 0;

pub fn init_page() {
    unsafe {
        PAGE_NUM = HEAP_SIZE / (PAGE_SIZE + size_of::<Page>());
        HDR_PAGE_END
        = round_up_align(HEAP_START + PAGE_NUM * size_of::<Page>(), 12);
        ALLOC_START = HDR_PAGE_END;
        ALLOC_END = ALLOC_START + PAGE_NUM * PAGE_SIZE;

        // clear all page header area to zero
        init_page_hdr();
    };
}


#[repr(u8)]
enum PageSpec {
    Empty = 0b_0000_0000,
    Taken = 0b_0000_0001,
    Last = 0b_0000_0010
}

impl PageSpec {
    pub fn val(self) -> u8 {
        self as u8
    }
}

pub struct Page {
    flags: u8
}

impl Page {
    pub fn is_last(&self) -> bool {
        self.flags & PageSpec::Last.val() != 0
    }

    pub fn is_taken(&self) -> bool {
        self.flags & PageSpec::Taken.val() != 0
    }

    #[inline]
    pub fn is_free(&self) -> bool {
        !self.is_taken()
    }

    pub fn clear(&mut self) {
        self.flags = PageSpec::Empty.val();
    }
}

impl BitOrAssign<PageSpec> for Page {
    fn bitor_assign(&mut self, rhs: PageSpec) {
        self.flags |= rhs.val()
    }
}

extern "C" {
    fn dw_clear_zero(addr: *const u64);
    fn dw_clear_one(addr: *const u64);
}

fn init_page_hdr() {
    unsafe {
        // 四种HDR Page清零逻辑的实现
        // 1. utils.rs clear_zero
        // 2. u64 ptr = 0
        // 3. embeded asm in rust
        // 4. call function defined in utils.S
        // 如果有支持riscv向量化操作的模块(riscv-v)
        // 直接调用向量化操作更好

        // 1.
        // let ptr = HEAP_START as *const u8;
        // clear_zero(ptr, *PAGE_NUM);

        let ptr = HEAP_START as *mut u64;
        let divsion = 8 / size_of::<Page>();

        for i in 0..PAGE_NUM/divsion {
            // 2.
            //*ptr.add(i) = 0;

            // 4.
            // dw_clear_zero(ptr.add(i));

            // 3.
            asm!(
                "sd zero, 0({addr})",
                addr = in(reg) ptr.add(i),
                options(nostack)
            );
        }
    }
}

/// Allocate pages
/// return Allocated Memory Address
pub fn alloc(pages: usize) -> Option<*mut u8> {
    assert!(pages > 0);

    unsafe {
        let ptr = HEAP_START as *mut Page;

        for i in 0..PAGE_NUM - pages {
            let mut found = false;

            if (*ptr.add(i)).is_free() {
                found = true;

                for j in i..i + pages {
                    if (*ptr.add(j)).is_taken() {
                        found = false;
                        break;
                    }
                }
            }

            if found {
                for k in i..i + pages {
                    (*ptr.add(k)) |= PageSpec::Taken;
                }

                (*ptr.add(i + pages - 1)) |= PageSpec::Last;

                return Some((ALLOC_START + PAGE_SIZE * i) as *mut u8);
            }
        }
    }

    // there are no enough or enough contiguous memory
    None
}


/// Dealloc a page
pub fn dealloc(hdr_page_ptr: *mut u8) {
    unsafe {
        assert!(
            hdr_page_ptr as usize >= HEAP_START
             &&
            hdr_page_ptr as usize <= HDR_PAGE_END,
            "The page header(0x{:08x}) should be in valid Page Header address scope(0x{:08x} - 0x{:08x})",
            hdr_page_ptr as usize, HEAP_START, HDR_PAGE_END
        );

        assert!(
            (hdr_page_ptr as usize - HEAP_START) % size_of::<Page>() == 0,
            "The page header should be aligned with {} bytes",
            size_of::<Page>()
        );

        let mut p = hdr_page_ptr as *mut Page;
        while (*p).is_taken() && !(*p).is_last() {
            (*p).clear();
            p = p.add(1);
        }

        assert!(
            (*p).is_last() == true,
            "Double Free Error (Not taken found before last)"
        );

        (*p).clear();
    }
}


/// Alloc and zero pages
/// return Allocated Memory Address
pub fn zalloc(pages: usize) -> Option<*mut u8> {
    if let Some(hdr_page_ptr) = alloc(pages) {
        // 有条件确实应该向量化
        for i in 0..(pages * PAGE_SIZE) / 16 {
            // TODO: 看看128位的代码
            unsafe {
                *((hdr_page_ptr as *mut u128).add(i)) = 0
            }
        }

        Some(hdr_page_ptr)
    } else {
        None
    }
}


/// Print all page allocations
/// This is mainly used for debugging.
pub unsafe fn print_page_allocation() {
    let mut hdr_page_ptr = HEAP_START as *const Page;
    let hdr_page_end = hdr_page_ptr.add(PAGE_NUM);

    println!();
    println!(
        r#"PAGE ALLOCATION TABLE
HDR_PAGE: {:p} -> {:p}
PHYS: 0x{:x} -> 0x{:x}"#,
hdr_page_ptr, hdr_page_end, ALLOC_START, ALLOC_END
    );
    println!("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    let mut taken_pages = 0;
    while hdr_page_ptr < hdr_page_end {
        if (*hdr_page_ptr).is_taken() {
            let mem_page_addr
            = ALLOC_START + (hdr_page_ptr as usize - HEAP_START) * PAGE_SIZE;

            print!("0x{:x} => ", mem_page_addr);
            let last_taken_pages = taken_pages;
            loop {
                taken_pages += 1;
                if (*hdr_page_ptr).is_last() {
                    let mem_page_ptr
                    = ALLOC_START +
                      (hdr_page_ptr as usize - HEAP_START) * PAGE_SIZE +
                      PAGE_SIZE - 1;  // extra 1 page

                    print!(
                            "0x{:x}: {:>3} page(s)",
                            mem_page_ptr,
                            (taken_pages - last_taken_pages)
                    );

                    println!(".");
                    break;
                }

                hdr_page_ptr = hdr_page_ptr.add(1);
            }
        }

        hdr_page_ptr = hdr_page_ptr.add(1);
    }

    println!("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    println!(
        "Allocated: {:>5} pages ({:>9} bytes).",
        taken_pages,
        taken_pages * PAGE_SIZE
    );
    println!(
        "Free     : {:>5} pages ({:>9} bytes).",
        PAGE_NUM - taken_pages,
        (PAGE_NUM - taken_pages) * PAGE_SIZE
    );
    println!();
}


///////////////////////////////////////////////////////////////////////////////
//// MMU

/// Table represents a single table, which contains 512 (2^9), 64-bit entries.
pub struct PageTable {
    pub entries: [Entry; 512]
}

impl PageTable {
    pub fn len() -> usize {
        512
    }
}

/// Sv39表项的设定
#[derive(Clone, Copy)]
#[repr(u64)]
pub enum EntrySpec {
    Empty   = 0,
    Valid   = 1 << 0,
    Read    = 1 << 1,
    Write   = 1 << 2,
    Execute = 1 << 3,
    User    = 1 << 4,
    Global  = 1 << 5,
    Access  = 1 << 6,
    Dirty   = 1 << 7,

    // convenience combination
    RW = Self::Read as u64 | Self::Write as u64,
    RX = Self::Read as u64 | Self::Execute as u64,
    RWX = Self::RW as u64 | Self::Execute as u64,
    URW = Self::User as u64 | Self::RW as u64,
    URX = Self::User as u64 | Self::RX as u64,
    URWX = Self::User as u64 | Self::RWX as u64
}


/// A single entry. We're using an i64 so that
/// this will sign-extend rather than zero-extend
/// since RISC-V requires that the reserved sections
/// take on the most significant bit.
pub struct Entry(i64);

impl Entry {
    pub fn is_valid(&self) -> bool {
        self.0 & EntrySpec::Valid as i64 != 0
    }

    // A leaf has one or more RWX bits set
    pub fn is_leaf(&self) -> bool {
        self.0 as u64 & EntrySpec::RWX as u64 != 0
    }

    pub fn is_branch(&self) -> bool {
        !self.is_leaf()
    }
}

pub fn get_vpn(vir_addr: usize) -> [usize; 3] {
    [
        // VPN[0] = vir_addr[20:12]
        (vir_addr >> 12) & 0x1ff,
        // VPN[1] = vir_addr[29:21]
        (vir_addr >> 21) & 0x1ff,
        // VPN[2] = vir_addr[38:30]
        (vir_addr >> 30) & 0x1ff
    ]
}

pub fn get_ppn(phy_addr: usize) -> [usize; 3] {
    [
        // PPN[0] = phy_addr[20:12]
        (phy_addr >> 12) & 0x1ff,
        // PPN[1] = phy_addr[29:21]
        (phy_addr >> 21) & 0x1ff,
        // PPN[2] = phy_addr[55:30]
        (phy_addr >> 30) & 0x3ff_ffff,  // 4 * 6 + 3 = 26 bit
    ]
}

/*
* get next entry which is pointed to by this entry
*/
pub fn get_nxt_lv_entry(entry: &Entry, next_vpn: usize) -> *mut Entry {

    let tmp_entry = ((entry.0 & !0x3ff) << 2) as *mut Entry;
    unsafe { tmp_entry.add(next_vpn) }
}


/// Map virtual address to physical address using JUST ONE PAGE (4KBytes)
pub fn map(
    root: &mut PageTable,
    vir_addr: usize,
    phy_addr: usize,
    entry_spec: EntrySpec,
    level: usize
)
{
    assert!(
        entry_spec as u64 & EntrySpec::RWX as u64 != 0,
        "Make sure the R, W, or X has been provided"
    );

    let vpn = get_vpn(vir_addr);
    let ppn = get_ppn(phy_addr);

    let mut entry = &mut root.entries[vpn[2]];

    // 1, 0 (>= level)
    for i in (level..2).rev() {
        if !entry.is_valid() {
            let page = zalloc(1).unwrap();
            // page 物理地址, i64 >> 2, 符号扩展右移两位:
            // Sv39 物理地址PPN从bit-12开始
            // 控制MMU的表项的PPN从bit-10开始
            // 表项赋值这个移位的地址值
            entry.0 = (page as i64 >> 2) | EntrySpec::Valid as i64;
        }

        // & !0x3ff 000..111(10个1)
        entry = unsafe {
            get_nxt_lv_entry(entry, vpn[i]).as_mut().unwrap()
        };
    }

    // We always encode ppn[2:0] no matter what level we're at.
    // This is fine for the page table entry, but it might lead to confusion.
    // Remember, if we stop at level 1, then PPN[0] is NOT copied from the PTE.
    // Instead, PPN[0] is copied from VPN[0].
    // This is performed by the hardware MMU.
    // In the end, we're just adding more information than might be necessary for a page table entry.
    let entry_value =
        (ppn[2] << 28) as i64 |  // PPN[2] = [53:28]
        (ppn[1] << 19) as i64 |  // PPN[1] = [27:19]
        (ppn[0] << 10) as i64 |  // PPN[0] = [18:10]
        entry_spec as i64 |
        EntrySpec::Valid as i64 |
        EntrySpec::Dirty as i64 |  // some machines require this
        EntrySpec::Access as i64;  // some machines require this

    entry.0 = entry_value;
}

/// Freeing MMU Tables
pub fn unmap(root: &mut PageTable) {
    // Start with level 2
    unsafe { _unmap(root, 2) };
}

unsafe fn _unmap(table: &mut PageTable, level: usize) {
    for index in 0..PageTable::len() {
        let entry = &table.entries[index];
        if entry.is_valid() && entry.is_branch() {
            let addr_nxtlv = (entry.0 & !0x3ff) << 2;
            let nxt_lv = level - 1;

            if nxt_lv == 0 {
                //dealloc(addr_nxtlv as *mut u8);
            } else {
                let table_nxtlv = (addr_nxtlv as *mut PageTable).as_mut().unwrap();
                _unmap(table_nxtlv, nxt_lv);
            }

            dealloc(addr_nxtlv as *mut u8);
        }
    }
}

/*
* Walking the page tables
*/
pub fn vir2phy(root: &PageTable, vir_addr: usize) -> Option<usize> {
    let vpn = get_vpn(vir_addr);

    let mut entry = &root.entries[vpn[2]];

    // 2, 1, 0
    for i in (0..=2).rev() {
        if !entry.is_valid() {
            // page fault
            break;
        }

        else if entry.is_leaf() {
            // Each PPN is 9
            // bits and they start at bit #12. So, our formula
            // bits = 12 + i * 9
            // SHL (bits - 1)
            // mask = SHL (bits - 1 + 1) - 1 (全是1)
            let offset_mask = (1 << (12 + 9 * i)) - 1;
            let vir_addr_page_offset = vir_addr & offset_mask;

            let addr
            = ((entry.0 << 2) as usize) & !offset_mask;  // align
            return Some(addr | vir_addr_page_offset);    // add page offset
        }

        entry = unsafe {
            get_nxt_lv_entry(entry, vpn[i-1]).as_ref().unwrap()
        };
    }

    None
}
