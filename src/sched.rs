use crate::cpu::get_mtime;
use crate::proc::{PROC_LIST, PROC_TURNABLE, ProcState, roll_proc};
use crate::println;

/// frame_addr
pub fn schedule() -> usize {
    let mut proc = roll_proc();

    println!("{:?}", proc);

    match proc.get_state() {
        ProcState::Running => {
            return proc.get_frame() as usize
        },
        ProcState::Sleeping => {
            if proc.get_sleep_until() <= get_mtime() {
                proc.set_state(ProcState::Running);

                return proc.get_frame() as usize;
            }
        },
        _ => {}
    }

    0
}
