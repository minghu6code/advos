#[macro_export]
macro_rules! print
{
    ($($args:tt)+) => ({
        use core::fmt::Write;
        use crate::uart::{ UART };
        use crate::r#const::{ UART0 };

        let _ = write!(UART::new(UART0), $($args)+);

    });
}

#[macro_export]
macro_rules! println
{
    () => ({
        use crate::print;
        print!("\r\n")
    });

    ($fmt:expr) => ({
        use crate::print;
        print!(concat!($fmt, "\r\n"))
    });

    ($fmt:expr, $($args:tt)+) => ({
        use crate::print;
        print!(concat!($fmt, "\r\n"), $($args)+)
    });
}
