use alloc::boxed::Box;
use alloc::collections::BinaryHeap;
use alloc::string::ToString;
use alloc::sync::Arc;
use core::cell::{RefCell, UnsafeCell};
use core::convert::TryInto;
use core::fmt;
use core::mem::{size_of, zeroed, MaybeUninit};
use core::ptr::null_mut;

use crate::cpu::{
    build_satp, get_mtime, mscratch_write, satp_fence_asid,
    satp_write, CPUMode, SatpMode, TrapFrame,
};
use crate::page::{self, EntrySpec, PageTable};
use crate::round_up_div;
use crate::syscall::{do_syscall, syscall_exit};
use crate::utils::{align8, u8ind, u8set, u8unset};
use crate::*;


///////////////////////////////////////////////////////////////////////////////
//// Constants

pub static PROC_LIST: KVar<BinaryHeap<Process>> = KVar::none();

trait ProcTurntable = FnMut() -> Process;

pub static mut PID_SPACE: *mut usize = null_mut();
pub static mut MAX_PID: usize = 0;
pub static mut PROC_TURNABLE: KVar<Box<dyn ProcTurntable>> = KVar::none();

/// one byte align, unit bytes
pub static mut PID_SPACE_LEN: usize = 0;

///////////////////////////////////////////////////////////////////////////////
//// Process

/// Running:  when the scheduler finds this process, it can run it.
/// Sleeping: the process is waiting on a certain amount of time.
/// Waiting: the process is waiting on I/O
/// Dead: We should never get here, but we can flag a process as Dead and clean
#[derive(Clone, Copy, Debug)]
pub enum ProcState {
    Running,
    Sleeping,
    Waiting,
    Dead,
}

#[derive(Clone, Copy)]
pub struct ProcData {
    cwd: [u8; 128],
}

impl ProcData {
    pub fn zero() -> Self {
        Self { cwd: [0; 128] }
    }
}

// Rust gets to choose how it orders
// the fields unless we represent the structure in
// C-style ABI.
#[derive(Clone)]
#[repr(C)]
pub struct Process {
    pub frame: *mut TrapFrame,
    pub stack: *mut u8,
    pub pid: pid_t,
    pub table: *mut PageTable,
    pub state: ProcState,
    pub data: ProcData,
    pub sleep_until: usize,
    pub prog: *mut u8,
}

impl PartialEq for Process {
    fn eq(&self, other: &Self) -> bool {
        unsafe {
            self.pid == other.pid
                && (*self.frame).pc == (*other.frame).pc
        }
    }
}

impl Eq for Process {}

impl PartialOrd for Process {
    fn partial_cmp(
        &self,
        other: &Self,
    ) -> Option<core::cmp::Ordering> {
        self.pid.partial_cmp(&other.pid)
    }
}
impl Ord for Process {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl Process {
    pub fn new_default() -> Process {
        let stack = page::alloc(PROC_STACK_PAGES).unwrap();
        let table = page::zalloc(1).unwrap() as *mut PageTable;
        let pid = gen_pid().unwrap();
        let state = ProcState::Running;
        let data = ProcData::zero();
        let sleep_until = 0;
        let prog = null_mut();

        let mut frame = page::zalloc(1).unwrap() as *mut TrapFrame;

        Self {
            frame,
            stack,
            pid,
            table,
            state,
            data,
            sleep_until,
            prog,
        }
    }

    pub fn new_kernel_proc(
        func: fn(),
    ) -> pid_t {
        let mut proc = Self::new_default();

        let pid = proc.pid;

        unsafe {
            /* Setup Trap Frame */

            // x1: ra
            (*proc.frame).regs[1] = syscall_exit as usize;

            // Now we move the stack pointer to the bottom of the allocation.
            // The spec shows that register x2 is the stack pointer(sp).
            (*proc.frame).regs[2] =
                proc.stack as usize + PAGE_SIZE * PROC_STACK_PAGES;

            (*proc.frame).mode = CPUMode::M as usize;
            (*proc.frame).pid = proc.pid as usize;

            (*proc.frame).pc = func as usize;

            /* Insert Process into PROC_LIST */
            insert_proc(proc);

            pid
        }
    }

    pub fn new_kernel_proc_args(
        func: fn(args_ptr: usize),
        args: usize,
    ) {
        let mut proc = Self::new_default();

        unsafe {
            /* Setup Trap Frame */

            // x1: ra
            (*proc.frame).regs[1] = syscall_exit as usize;

            // Now we move the stack pointer to the bottom of the allocation.
            // The spec shows that register x2 is the stack pointer(sp).
            (*proc.frame).regs[2] =
                proc.stack as usize + PAGE_SIZE * PROC_STACK_PAGES;

            (*proc.frame).mode = CPUMode::M as usize;
            (*proc.frame).pid = proc.pid as usize;

            (*proc.frame).pc = func as usize;

            (*proc.frame).regs[10] = args;

            /* Insert Process into PROC_LIST */
            insert_proc(proc);
        }
    }

    pub fn destroy(&mut self) {
        page::dealloc(self.stack);

        unsafe {
            page::unmap(&mut *self.table);
        }

        page::dealloc(self.table as *mut u8);
        page::dealloc(self.frame as *mut u8);

        if !self.prog.is_null() {
            page::dealloc(self.prog);
        }
    }


    ///////////////////////////////////////////////////////
    //// Getter

    pub fn get_frame(&self) -> *const TrapFrame {
        self.frame
    }

    pub fn get_frame_mut(&self) -> *mut TrapFrame {
        self.frame
    }

    pub fn get_pc(&self) -> usize {
        unsafe { (*self.frame).pc }
    }

    pub fn get_table_addr(&self) -> usize {
        self.table as usize
    }

    pub fn get_state(&self) -> ProcState {
        self.state.clone()
    }

    pub fn get_pid(&self) -> pid_t {
        self.pid
    }

    pub fn get_sleep_until(&self) -> usize {
        self.sleep_until
    }

    ///////////////////////////////////////////////////////
    //// Setter

    pub fn set_state(&mut self, state: ProcState) {
        self.state = state;
    }

    pub fn set_sleep_until(&mut self, sleep_until: usize) {
        self.sleep_until = sleep_until;
    }
}

impl Drop for Process {
    fn drop(&mut self) {
        self.destroy()
    }
}

impl fmt::Debug for Process {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "pid:   {}", self.pid)?;
        writeln!(f, "state: {:?}", self.state)?;
        // writeln!(f, "prog: {}", self.prog as usize)?;

        Ok(())
    }
}

/// Kernel Static Variable (thread unsafe)
#[derive(Debug)]
#[repr(transparent)]
pub struct KVar<T> {
    value: UnsafeCell<MaybeUninit<T>>,
}

unsafe impl<T> Sync for KVar<T> {}

impl<T> KVar<T> {
    pub const fn none() -> Self {
        Self {
            value: UnsafeCell::new(MaybeUninit::zeroed()),
        }
    }

    unsafe fn value(&self) -> &mut MaybeUninit<T> {
        &mut (*self.value.get())
    }

    pub unsafe fn write(&self, val: T) {
        (*self.value.get()).write(val);
    }

    pub unsafe fn get_mut(&self) -> &mut T {
        self.value().assume_init_mut()
    }

    pub unsafe fn get(&self) -> &T {
        self.value().assume_init_ref()
    }

    pub unsafe fn read(&self) -> T {
        self.value().assume_init_read()
    }

}

///////////////////////////////////////////////////////////////////////////////
//// Pid Alloc/Free


/// Get the next pid
pub fn gen_pid() -> Option<pid_t> {
    unsafe {
        let mut ptr = PID_SPACE;
        let end = (PID_SPACE as usize + PID_SPACE_LEN);

        // 64
        while (ptr as usize) < end && *ptr ^ usize::MAX == 0 {
            ptr = ptr.add(1);
        }
        if (ptr as usize) >= end {
            ptr = ptr.sub(1);
        }

        // 8
        let mut u8ptr = ptr as *mut u8;
        while (u8ptr as usize) < end && *u8ptr ^ u8::MAX == 0 {
            u8ptr = u8ptr.add(1);
        }
        if (u8ptr as usize) >= end {
            return None;
        }

        // 1
        let mut u8offset = 0;
        while u8ind(*u8ptr, u8offset) {
            u8offset += 1
        }
        u8set(u8ptr, u8offset);

        Some(
            ((u8ptr as usize - PID_SPACE as usize) * 8
                + u8offset as usize
                + 1) as pid_t,
        )
    }
}

pub fn recycle_pid(pid: pid_t) -> Result<(), TrapError> {
    let offset_bytes = (pid as usize) / 8;
    let offset_bits = ((pid as usize) % 8) as u8;

    unsafe {
        let mut ptr = PID_SPACE as *mut u8;
        ptr = ptr.add(offset_bytes);

        if !u8ind(*ptr, offset_bits) {
            return Err(TrapError {
                code: TrapCode::PidDoubleRecycle,
                msg: format!("ind:{}, {:08b}", offset_bits, *ptr),
            });
        }

        u8unset(ptr, offset_bits);
    }

    Ok(())
}

fn init_pid_space() {
    unsafe {
        MAX_PID = (((1u128 << (size_of::<pid_t>() * 8)) - 1)
            as usize)
            .try_into()
            .unwrap();
        PID_SPACE_LEN = (MAX_PID + 1) / 8; // pid start from 1.

        let pages = round_up_div!(PID_SPACE_LEN, PAGE_SIZE);
        PID_SPACE = page::zalloc(pages).unwrap() as *mut usize;
    }
}

///////////////////////////////////////////////////////////////////////////////
//// Process Init

pub fn init_proc() {
    unsafe {
        PROC_LIST.write(BinaryHeap::with_capacity(5));
        init_pid_space();

        let pid = Process::new_kernel_proc(proc0);
        PROC_TURNABLE.write(gen_pid_turntable(pid));

        let fstframe =
            PROC_LIST.get().peek().unwrap_unchecked().get_frame();
        println!("Init's frame is at 0x{:08x}", fstframe as usize);
    }
}

fn proc0() {
    // We can't do much here until we have system calls because
    // we're running in User space.
    // WARNING: we also can't use uart print!

    println!("Init process started ...");

    loop {
        for _ in 0..500 {
            unsafe { asm!("wfi") }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//// Process Time Slicing

pub fn roll_proc() -> Process {
    unsafe {
        PROC_TURNABLE.get_mut()()
    }
}

// time slicing cycle
pub fn gen_pid_turntable(init_pid: pid_t) -> Box<dyn ProcTurntable> {
    let mut cur_pid = init_pid;

    Box::new(move || unsafe {
        let proc_list = PROC_LIST.read().into_sorted_vec();

        let mut nxt_proc;
        if let Ok(idx) =
            proc_list.binary_search_by_key(&cur_pid, |p| p.get_pid())
        {
            if idx == 0 {
                nxt_proc = proc_list[proc_list.len() - 1].clone();
            } else {
                nxt_proc = proc_list[idx - 1].clone();
            }
        } else {
            unreachable!()
        }

        cur_pid = nxt_proc.get_pid();
        PROC_LIST.write(BinaryHeap::from(proc_list));

        nxt_proc
    })
}

///////////////////////////////////////////////////////////////////////////////
//// Process Other Helper

pub fn get_proc(pid: pid_t) -> Option<Process> {
    unsafe {
        let proc_list = PROC_LIST.read().into_sorted_vec();
        let res;

        if let Ok(idx) =
            proc_list.binary_search_by_key(&pid, |p| p.get_pid())
        {
            res = Some(proc_list.get(idx).unwrap().clone())
        } else {
            res = None
        }

        PROC_LIST.write(BinaryHeap::from(proc_list));

        res
    }
}

pub fn insert_proc(proc: Process) {
    unsafe {
        PROC_LIST.get_mut().push(proc);
    }
}

/// Remove Process from PROC_LIST, this invoke also trigger the process destroy...
/// because of impl Drop for Process
pub fn rm_proc(pid: pid_t) {
    unsafe {
        let mut proc_list =
            PROC_LIST.read().into_sorted_vec();

        if let Ok(idx) =
            proc_list.binary_search_by_key(&pid, |p| p.get_pid())
        {
            proc_list.remove(idx);
        }

        PROC_LIST.write(BinaryHeap::from(proc_list));
    }
}

pub fn set_running(pid: pid_t) {
    if let Some(mut proc) = get_proc(pid) {
        proc.set_state(ProcState::Running)
    }
}

pub fn set_waiting(pid: pid_t) {
    if let Some(mut proc) = get_proc(pid) {
        proc.set_state(ProcState::Waiting)
    }
}

pub fn set_sleeping(pid: pid_t, duration: usize) {
    if let Some(mut proc) = get_proc(pid) {
        proc.set_state(ProcState::Sleeping);
        proc.set_sleep_until(get_mtime() + duration);
    }
}

///////////////////////////////////////////////////////////////////////////////
//// Test
#[cfg(feature = "customtest")]
pub mod customtest {
    use super::*;
    use crate::*;

    pub fn test_pid_bitmap() {
        unsafe {
            // Alloc all pid
            let mut pid_vec = vec![0; MAX_PID];

            for proc in PROC_LIST.get().iter() {
                print!("{}..", proc.get_pid());
            }

            for i in 1..=MAX_PID {
                if let Some(pid) = gen_pid() {
                    // recycle_pid(i as pid_t);
                    pid_vec.push(pid);
                } else {
                    panic!(
                        "Generate Pid(max: {}) Failed when: {}",
                        MAX_PID, i
                    );
                }
            }
            print!("Regular Generate MAX PID succeed...");

            for i in (1..=MAX_PID).rev() {
                if let Err(err) = recycle_pid(i as pid_t) {
                    //panic!("code: {:?}, {}", code, i);
                    print!("code: {:?}, {}", err, i);
                }
            }
            println!("Regular Recycle MAX PID succeed...");
        }
    }
}
