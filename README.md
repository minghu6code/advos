# Learning from Adventure of OS

ref: https://osblog.stephenmarz.com/ch0.html

## Development Requirements

Linux build for riscv64

```bash
CC=$(CROSS_COMPILE)g++
OBJDUMP=$(CROSS_COMPILE)objdump
QEMU=qemu-system-riscv64
```

### Ubuntu-24.04

`sudo apt install gcc-riscv64-linux-g++ qemu-system-misc`

## Memory Model

### Physical Memory Model

Paging System

```

| ------------------------ ~4K x 32K = 128M ------------------------ |
[ --- page header --- | ---------------- rem physical memory ------- ]
|                     |                                              |
HEAP_START            HDR_PAGE_END                                   HEAP_END
| - 64 - |            | - 64 x 4k - |
hdr_kernel                          MEM_K_END
(used for global allocator)         |
                                    | 1 x 4k |
                                             MEM_K_PT
                                             (root table)

64 or 512 or anyway
```

### Virtual Memory Model

Sv39 plan: 3(level) x 9(bit) + 12(offset)

```
|      4K (one page)       |                                         |
|    512 x 8K (one table)  |                                         HEAP_END
| ... | entry-vpn[2] | ... | entry-vpn[1] | ... | entry-vpn[0] | ... |
       1G Bytes page ------> 2M Bytes page ------> 4K Bytes page
                 (if not leaf)         (if not leaf)
```

Get next level entry from this entry:

1. this entry clear 10 bit && Shift Left 2 bit
2. add vpn[level - 1] get next entry address
