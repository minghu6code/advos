###############################################################################
#### BUILD
CROSS_COMPILE=riscv64-linux-gnu-
CC=$(CROSS_COMPILE)g++
CFLAGS=-Wall -Wextra -pedantic -Wextra -O2 -g -std=c++17
CFLAGS+=-static -ffreestanding -nostdlib -fno-rtti -fno-exceptions
CFLAGS+=-march=rv64gc -mabi=lp64d  # 64d: 'd': pass float && pass double foloat
INCLUDES=
LINKER_SCRIPT=-Tsrc/lds/virt.lds
TYPE=debug
RUST_TARGET=./target/riscv64gc-unknown-none-elf/$(TYPE)
LIBS=-L$(RUST_TARGET)
SOURCES_ASM=$(wildcard src/asm/*.S)
LIB=-ladvos -lgcc  # advos: proj name
OUT=os.elf

###############################################################################
#### Tools
OBJDUMP=$(CROSS_COMPILE)objdump
# GDB=$(CROSS_COMPILE)gdb
GDB=./rust-gdb  # $CROSS_COMPILE需要与rust-gdb中的同步
###############################################################################
#### QEMU
QEMU=qemu-system-riscv64
MACH=virt
CPU=rv64
CPUS=4
MEM=128M
DRIVE=hdd.dsk
QFLAGS=-machine $(MACH) -bios fw_dynamic.bin -cpu $(CPU) -smp $(CPUS) -m $(MEM)\
 -nographic -serial mon:stdio -kernel $(OUT)\
 -drive if=none,format=raw,file=$(DRIVE),id=foo -device virtio-blk-device,scsi=off,drive=foo\
 -device virtio-rng-device -device virtio-gpu-device\
 -device virtio-net-device -device virtio-tablet-device -device virtio-keyboard-device


all:
	cargo build
	$(CC) $(CFLAGS) $(LINKER_SCRIPT) $(INCLUDES) -o $(OUT) $(SOURCES_ASM) $(LIBS) $(LIB)

run: all
	$(QEMU) $(QFLAGS)

test:
	cargo build --features "customtest"
	$(CC) $(CFLAGS) $(LINKER_SCRIPT) $(INCLUDES) -o $(OUT) $(SOURCES_ASM) $(LIBS) $(LIB)
	$(QEMU) $(QFLAGS)

dump:
	@${OBJDUMP} -S os.elf | less

# qemu -s: shorthand for -gdb tcp::1234
# qemu -S: freeze CPU at startup (use 'c' to start execution)
.PHONY: debug
debug: all
	@echo "-------------------------------------------------------"
	@${QEMU} ${QFLAGS} -s -S &
	@${GDB} os.elf -q -x ./gdbinit

.PHONY: clean
clean:
	cargo clean
	rm -f $(OUT)

.PHONY: kill_qemu
kill_qemu:
	@pidof $(QEMU) | xargs kill -s 9

.PHONY: make_minix3_hdd
make_minix3_hdd:
	fallocate  -l 32M hdd.dsk
	sudo losetup /dev/loop41 hdd.dsk   # or any empty loop device
	sudo mkfs.minix -3 /dev/loop41
	sudo mount /dev/loop41 /mnt
	echo "Hello, this is my first file on Minix's filesystem" | sudo tee /mnt/hello.txt
	stat /mnt/hello.txt
	sudo sync /mnt

.PHONY: rm_minix3_hdd
rm_minix3_hdd:
	rm hdd.dsk
	sudo umount /mnt
	sudo losetup -d /dev/loop41
